using Unity.XR.PXR;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public Text debugText;
    public RawImage rawImageAntiDistortion;
    public GameObject HMDCamRed;  // 红色，显示 Camera.main.transform globalPose 数据来源head globalPose
    public GameObject APICamBlue; // 蓝色，显示上屏时刻的预测 head globalPose
    public GameObject RGBCamGreen;// 绿色，显示 AcquireVSTCameraFrameAntiDistortion接口获取图像时刻的 head globalPose

    private Texture2D texture2D;
    private bool enableAcquireVSTCameraFrameAntiDistortion = false;
    private int width = 2328;
    private int height = 1748;
    private Vector3 zOffset = new Vector3(0, -0.5f, 0.5f);
    private string TAG = "Test ";


    private void Start()
    {
        PXR_Boundary.EnableSeeThroughManual(true);          // 打开全局seethrgouth
        PXR_Plugin.Boundary.UPxr_SetSeeThroughState(true); // 设置全局坐标系(Unity Camera.main.transform 数据来源：head globalPose，此时与AcquireVSTCameraFrameAntiDistortion返回值 frame.rgbCameraPose)

        PXR_Enterprise.InitEnterpriseService();         // 初始化

        texture2D = new Texture2D(width, height, TextureFormat.RGB24, false, false);
    }

    private void Update()
    {
        if (enableAcquireVSTCameraFrameAntiDistortion)
        {
            AcquireVSTCameraFrameAntiDistortionIntPtr();
        }

        // 显示MainCamera位姿
        Vector3 pos1 = Camera.main.transform.position + zOffset;
        Quaternion quat1 = Camera.main.transform.rotation;
        HMDCamRed.GetComponent<Transform>().position = pos1;
        HMDCamRed.GetComponent<Transform>().rotation = quat1;

        // 获取上屏时刻的预测时间戳
        double predictTime = PXR_Enterprise.GetPredictedDisplayTime();
        // 获取上屏时刻的sensor 状态和位姿
        SensorState sensorState = PXR_Enterprise.GetPredictedMainSensorState(predictTime);
        // 显示上屏时刻预测位姿
        APICamBlue.GetComponent<Transform>().position = PXR_EnterprisePlugin.ToUnityPose(sensorState.pose).position + zOffset;
        APICamBlue.GetComponent<Transform>().rotation = PXR_EnterprisePlugin.ToUnityPose(sensorState.pose).rotation;

        Debug.Log("HHHH SensorState status="+ sensorState.status);
    }

    void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            PXR_Boundary.EnableSeeThroughManual(true);
        }
    }

    private void OnDisable()
    {
        texture2D = null;
        PXR_Enterprise.CloseVSTCamera();
    }

    public void OpenVSTCamera()
    {
        bool result = PXR_Enterprise.OpenVSTCamera();
        debugText.text = "OpenVSTCamera :" + result;
    }

    public void CloseVSTCamera()
    {
        bool result = PXR_Enterprise.CloseVSTCamera();
        enableAcquireVSTCameraFrameAntiDistortion = false;
        debugText.text = "CloseVSTCamera :" + result;
    }

    // 获取图像
    public void AcquireVSTCameraFrameAntiDistortionIntPtr()
    {
        try
        {
            Frame frame = PXR_Enterprise.AcquireVSTCameraFrameAntiDistortion(width, height);

            texture2D.LoadRawTextureData(frame.data, (int)frame.datasize);
            texture2D.Apply();
            rawImageAntiDistortion.texture = texture2D;

            enableAcquireVSTCameraFrameAntiDistortion = true;

            RGBCamGreen.GetComponent<Transform>().position = PXR_EnterprisePlugin.ToUnityPose(frame.pose).position + zOffset;
            RGBCamGreen.GetComponent<Transform>().rotation = PXR_EnterprisePlugin.ToUnityPose(frame.pose).rotation;


            Debug.LogFormat(TAG + "AcquireVSTCameraFrameAntiDistortion frame.data = {0},  datasize = {1}, post=({2},{3},{4}, rotation=({5},{6},{7},{8})", frame.data, frame.datasize, frame.pose.position.x, frame.pose.position.y, frame.pose.position.z,
                frame.pose.rotation.x, frame.pose.rotation.y, frame.pose.rotation.z, frame.pose.rotation.w);
        }
        catch (System.Exception e)
        {
            Debug.LogFormat(TAG + " e={0}", e);
            throw;
        }
    }

    public void GetHeadTrackingConfidence()
    {
        width--;
        texture2D = null;
        texture2D = new Texture2D(width, height, TextureFormat.RGB24, false, false);
        int result = PXR_Enterprise.GetHeadTrackingConfidence();
        debugText.text = "GetHeadTrackingConfidence :" + result;
    }

    public void GetCameraParameters()
    {
        PXR_Enterprise.GetCameraParameters();
    }
}
