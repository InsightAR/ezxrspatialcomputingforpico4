// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FBH/FBH_FX_portal2"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_VertexOffset("VertexOffset", Float) = 0
		_RoSpeed("RoSpeed", Float) = 0
		_RoSpeed2("RoSpeed2", Float) = 0
		_Bright("Bright", Float) = 0
		_ABright("ABright", Float) = 10
		[HDR]_AColor0("AColor 0", Color) = (0,0,0,0)
		_NoiseScale("NoiseScale", Float) = 0
		_NoiseRoSpeed("NoiseRoSpeed", Float) = 0
		_NoiseSpeed("NoiseSpeed", Float) = 0
		_Opacity("Opacity", Float) = 0
		[KeywordEnum(Key0,Key1,Key2)] _ColorSwitch("ColorSwitch", Float) = 0
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_2Str("2Str", Float) = 0
		_NoiseMu("NoiseMu", Float) = 0
		_Mask("Mask", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _COLORSWITCH_KEY0 _COLORSWITCH_KEY1 _COLORSWITCH_KEY2
		#pragma only_renderers d3d11 glcore gles gles3 metal 
		#pragma surface surf Unlit keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MainTex;
		uniform float _RoSpeed;
		uniform float _NoiseSpeed;
		uniform float _NoiseScale;
		uniform float _NoiseMu;
		uniform float _RoSpeed2;
		uniform float _2Str;
		uniform float _NoiseRoSpeed;
		uniform float _VertexOffset;
		uniform sampler2D _Mask;
		uniform float4 _Mask_ST;
		uniform float _Bright;
		sampler2D _Sampler60139;
		uniform float4 _AColor0;
		uniform float _ABright;
		uniform sampler2D _TextureSample1;
		sampler2D _Sampler60156;
		uniform float _Opacity;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		struct Gradient
		{
			int type;
			int colorsLength;
			int alphasLength;
			float4 colors[8];
			float2 alphas[8];
		};


		Gradient NewGradient(int type, int colorsLength, int alphasLength, 
		float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
		float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
		{
			Gradient g;
			g.type = type;
			g.colorsLength = colorsLength;
			g.alphasLength = alphasLength;
			g.colors[ 0 ] = colors0;
			g.colors[ 1 ] = colors1;
			g.colors[ 2 ] = colors2;
			g.colors[ 3 ] = colors3;
			g.colors[ 4 ] = colors4;
			g.colors[ 5 ] = colors5;
			g.colors[ 6 ] = colors6;
			g.colors[ 7 ] = colors7;
			g.alphas[ 0 ] = alphas0;
			g.alphas[ 1 ] = alphas1;
			g.alphas[ 2 ] = alphas2;
			g.alphas[ 3 ] = alphas3;
			g.alphas[ 4 ] = alphas4;
			g.alphas[ 5 ] = alphas5;
			g.alphas[ 6 ] = alphas6;
			g.alphas[ 7 ] = alphas7;
			return g;
		}


		float4 SampleGradient( Gradient gradient, float time )
		{
			float3 color = gradient.colors[0].rgb;
			UNITY_UNROLL
			for (int c = 1; c < 8; c++)
			{
			float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
			color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
			}
			#ifndef UNITY_COLORSPACE_GAMMA
			color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
			#endif
			float alpha = gradient.alphas[0].x;
			UNITY_UNROLL
			for (int a = 1; a < 8; a++)
			{
			float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
			alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
			}
			return float4(color, alpha);
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 uv_TexCoord161 = v.texcoord.xy + float2( -0.5,-0.5 );
			float mulTime84 = _Time.y * _RoSpeed;
			float cos81 = cos( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float sin81 = sin( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float2 rotator81 = mul( v.texcoord.xy - float2( 0.5,0.5 ) , float2x2( cos81 , -sin81 , sin81 , cos81 )) + float2( 0.5,0.5 );
			float mulTime130 = _Time.y * _NoiseSpeed;
			float cos129 = cos( mulTime130 );
			float sin129 = sin( mulTime130 );
			float2 rotator129 = mul( v.texcoord.xy - float2( 0.5,0.5 ) , float2x2( cos129 , -sin129 , sin129 , cos129 )) + float2( 0.5,0.5 );
			float simplePerlin2D125 = snoise( rotator129*_NoiseScale );
			simplePerlin2D125 = simplePerlin2D125*0.5 + 0.5;
			float DisNoise133 = (-_NoiseMu + (simplePerlin2D125 - 0.0) * (_NoiseMu - -_NoiseMu) / (1.0 - 0.0));
			float mulTime89 = _Time.y * _RoSpeed2;
			float cos87 = cos( mulTime89 );
			float sin87 = sin( mulTime89 );
			float2 rotator87 = mul( v.texcoord.xy - float2( 0.5,0.5 ) , float2x2( cos87 , -sin87 , sin87 , cos87 )) + float2( 0.5,0.5 );
			float4 tex2DNode88 = tex2Dlod( _MainTex, float4( ( rotator87 + DisNoise133 ), 0, 0.0) );
			float mulTime100 = _Time.y * _NoiseRoSpeed;
			float cos98 = cos( mulTime100 );
			float sin98 = sin( mulTime100 );
			float2 rotator98 = mul( v.texcoord.xy - float2( 0.5,0.5 ) , float2x2( cos98 , -sin98 , sin98 , cos98 )) + float2( 0.5,0.5 );
			float simplePerlin2D96 = snoise( rotator98*3.0 );
			simplePerlin2D96 = simplePerlin2D96*0.5 + 0.5;
			float lerpResult94 = lerp( tex2Dlod( _MainTex, float4( ( rotator81 + DisNoise133 ), 0, 0.0) ).a , ( tex2DNode88.a * _2Str ) , simplePerlin2D96);
			float temp_output_116_0 = pow( lerpResult94 , 1.2 );
			float3 appendResult55 = (float3(0.0 , ( temp_output_116_0 * _VertexOffset ) , 0.0));
			v.vertex.xyz += appendResult55;
			v.vertex.w = 1;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			Gradient gradient103 = NewGradient( 0, 4, 2, float4( 0.04685831, 0.1520352, 0.3679245, 0 ), float4( 0.1112467, 0.5721589, 0.8204594, 0.7942016 ), float4( 0.3760715, 0.8649399, 1, 0.915938 ), float4( 0.4292453, 0.8929623, 1, 1 ), 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float2 uv_Mask = i.uv_texcoord * _Mask_ST.xy + _Mask_ST.zw;
			float4 tex2DNode41 = tex2D( _Mask, uv_Mask );
			Gradient gradient141 = NewGradient( 0, 3, 2, float4( 0.03137255, 0.1574734, 0.254902, 0 ), float4( 0.1137255, 0.5165758, 0.8862745, 0.4057984 ), float4( 0.6934233, 0.4386792, 1, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			Gradient gradient142 = NewGradient( 0, 3, 2, float4( 0.1681589, 0.1551264, 0.3962264, 0 ), float4( 0.1137255, 0.4503766, 0.8862745, 0.6 ), float4( 0, 0.6698114, 0.8018868, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			#if defined(_COLORSWITCH_KEY0)
				float4 staticSwitch143 = SampleGradient( gradient103, tex2DNode41.r );
			#elif defined(_COLORSWITCH_KEY1)
				float4 staticSwitch143 = SampleGradient( gradient141, tex2DNode41.r );
			#elif defined(_COLORSWITCH_KEY2)
				float4 staticSwitch143 = SampleGradient( gradient142, tex2DNode41.r );
			#else
				float4 staticSwitch143 = SampleGradient( gradient103, tex2DNode41.r );
			#endif
			float2 temp_output_1_0_g2 = float2( 1,1 );
			float2 appendResult10_g2 = (float2(( (temp_output_1_0_g2).x * i.uv_texcoord.x ) , ( i.uv_texcoord.y * (temp_output_1_0_g2).y )));
			float2 temp_output_11_0_g2 = float2( 0,0 );
			float2 panner18_g2 = ( ( (temp_output_11_0_g2).x * _Time.y ) * float2( 1,0 ) + i.uv_texcoord);
			float2 panner19_g2 = ( ( _Time.y * (temp_output_11_0_g2).y ) * float2( 0,1 ) + i.uv_texcoord);
			float2 appendResult24_g2 = (float2((panner18_g2).x , (panner19_g2).y));
			float2 temp_output_47_0_g2 = float2( -0.05,0 );
			float2 uv_TexCoord78_g2 = i.uv_texcoord * float2( 2,2 );
			float2 temp_output_31_0_g2 = ( uv_TexCoord78_g2 - float2( 1,1 ) );
			float2 appendResult39_g2 = (float2(frac( ( atan2( (temp_output_31_0_g2).x , (temp_output_31_0_g2).y ) / 6.28318548202515 ) ) , length( temp_output_31_0_g2 )));
			float2 panner54_g2 = ( ( (temp_output_47_0_g2).x * _Time.y ) * float2( 1,0 ) + appendResult39_g2);
			float2 panner55_g2 = ( ( _Time.y * (temp_output_47_0_g2).y ) * float2( 0,1 ) + appendResult39_g2);
			float2 appendResult58_g2 = (float2((panner54_g2).x , (panner55_g2).y));
			float simplePerlin2D115 = snoise( ( ( (tex2D( _Sampler60139, ( appendResult10_g2 + appendResult24_g2 ) )).rg * 1.0 ) + ( float2( 0.8,1 ) * appendResult58_g2 ) )*3.74 );
			simplePerlin2D115 = simplePerlin2D115*0.5 + 0.5;
			float smoothstepResult114 = smoothstep( 0.75 , 1.0 , simplePerlin2D115);
			float mulTime89 = _Time.y * _RoSpeed2;
			float cos87 = cos( mulTime89 );
			float sin87 = sin( mulTime89 );
			float2 rotator87 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos87 , -sin87 , sin87 , cos87 )) + float2( 0.5,0.5 );
			float mulTime130 = _Time.y * _NoiseSpeed;
			float cos129 = cos( mulTime130 );
			float sin129 = sin( mulTime130 );
			float2 rotator129 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos129 , -sin129 , sin129 , cos129 )) + float2( 0.5,0.5 );
			float simplePerlin2D125 = snoise( rotator129*_NoiseScale );
			simplePerlin2D125 = simplePerlin2D125*0.5 + 0.5;
			float DisNoise133 = (-_NoiseMu + (simplePerlin2D125 - 0.0) * (_NoiseMu - -_NoiseMu) / (1.0 - 0.0));
			float4 tex2DNode88 = tex2D( _MainTex, ( rotator87 + DisNoise133 ) );
			float2 temp_output_1_0_g4 = float2( 1,1 );
			float2 appendResult10_g4 = (float2(( (temp_output_1_0_g4).x * i.uv_texcoord.x ) , ( i.uv_texcoord.y * (temp_output_1_0_g4).y )));
			float2 temp_output_11_0_g4 = float2( 0,0 );
			float2 panner18_g4 = ( ( (temp_output_11_0_g4).x * _Time.y ) * float2( 1,0 ) + i.uv_texcoord);
			float2 panner19_g4 = ( ( _Time.y * (temp_output_11_0_g4).y ) * float2( 0,1 ) + i.uv_texcoord);
			float2 appendResult24_g4 = (float2((panner18_g4).x , (panner19_g4).y));
			float2 temp_output_47_0_g4 = float2( -0.1,0 );
			float2 uv_TexCoord78_g4 = i.uv_texcoord * float2( 2,2 );
			float2 temp_output_31_0_g4 = ( uv_TexCoord78_g4 - float2( 1,1 ) );
			float2 appendResult39_g4 = (float2(frac( ( atan2( (temp_output_31_0_g4).x , (temp_output_31_0_g4).y ) / 6.28318548202515 ) ) , length( temp_output_31_0_g4 )));
			float2 panner54_g4 = ( ( (temp_output_47_0_g4).x * _Time.y ) * float2( 1,0 ) + appendResult39_g4);
			float2 panner55_g4 = ( ( _Time.y * (temp_output_47_0_g4).y ) * float2( 0,1 ) + appendResult39_g4);
			float2 appendResult58_g4 = (float2((panner54_g4).x , (panner55_g4).y));
			o.Emission = ( ( staticSwitch143 * _Bright ) + ( ( smoothstepResult114 * _AColor0 ) * _ABright * tex2DNode88.a * tex2DNode41.r * tex2D( _TextureSample1, ( ( (tex2D( _Sampler60156, ( appendResult10_g4 + appendResult24_g4 ) )).rg * 1.0 ) + ( float2( 2,3 ) * appendResult58_g4 ) ) ).a ) ).rgb;
			float2 uv_TexCoord161 = i.uv_texcoord + float2( -0.5,-0.5 );
			float mulTime84 = _Time.y * _RoSpeed;
			float cos81 = cos( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float sin81 = sin( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float2 rotator81 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos81 , -sin81 , sin81 , cos81 )) + float2( 0.5,0.5 );
			float mulTime100 = _Time.y * _NoiseRoSpeed;
			float cos98 = cos( mulTime100 );
			float sin98 = sin( mulTime100 );
			float2 rotator98 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos98 , -sin98 , sin98 , cos98 )) + float2( 0.5,0.5 );
			float simplePerlin2D96 = snoise( rotator98*3.0 );
			simplePerlin2D96 = simplePerlin2D96*0.5 + 0.5;
			float lerpResult94 = lerp( tex2D( _MainTex, ( rotator81 + DisNoise133 ) ).a , ( tex2DNode88.a * _2Str ) , simplePerlin2D96);
			float temp_output_116_0 = pow( lerpResult94 , 1.2 );
			o.Alpha = ( _Opacity * temp_output_116_0 );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
103;45;1676;908;957.2003;558.4833;1.099002;True;False
Node;AmplifyShaderEditor.RangedFloatNode;128;-1118.237,2012.785;Inherit;False;Property;_NoiseSpeed;NoiseSpeed;15;0;Create;True;0;0;0;False;0;False;0;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;130;-938.4914,2058.844;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;126;-1004.237,1851.785;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;127;-747.2364,2142.785;Inherit;False;Property;_NoiseScale;NoiseScale;13;0;Create;True;0;0;0;False;0;False;0;7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;134;-595.4914,2265.843;Inherit;False;Property;_NoiseMu;NoiseMu;20;0;Create;True;0;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;129;-764.4914,1986.844;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NegateNode;135;-454.4914,2227.843;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;161;-1428.514,969.8489;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;125;-562.2559,2014.893;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;162;-1192.812,966.6102;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;131;-334.4914,2068.844;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-1034.724,1452.97;Inherit;False;Property;_RoSpeed2;RoSpeed2;9;0;Create;True;0;0;0;False;0;False;0;-0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-1154.048,1157.53;Inherit;False;Property;_RoSpeed;RoSpeed;8;0;Create;True;0;0;0;False;0;False;0;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;84;-997.0485,1160.53;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;133;-143.4914,2098.844;Inherit;False;DisNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;86;-1014.724,1253.97;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;164;-1051.812,980.6102;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;89;-876.7241,1435.97;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;99;-715.1819,1725.029;Inherit;False;Property;_NoiseRoSpeed;NoiseRoSpeed;14;0;Create;True;0;0;0;False;0;False;0;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;82;-987.0485,856.5305;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;160;-844.812,1045.61;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;-665.351,1198.792;Inherit;False;133;DisNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;87;-742.6306,1310.365;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;97;-663.1819,1556.029;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;124;-500.6344,1339.298;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;81;-701.955,975.9249;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;100;-499.1863,1728.053;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;-354.0912,1290.998;Inherit;True;Property;_MainTex2;MainTex2;6;0;Create;True;0;0;0;False;0;False;-1;1b58d66854585cb45a49a0048e0c0e95;1b58d66854585cb45a49a0048e0c0e95;True;0;False;white;Auto;False;Instance;35;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;123;-459.005,1100.78;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;102;-199.1818,1493.029;Inherit;False;Property;_2Str;2Str;19;0;Create;True;0;0;0;False;0;False;0;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;98;-278.2497,1622.005;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;139;-420.558,220.5871;Inherit;False;RadialUVDistortion;-1;;2;051d65e7699b41a4c800363fd0e822b2;0;7;60;SAMPLER2D;_Sampler60139;False;1;FLOAT2;1,1;False;11;FLOAT2;0,0;False;65;FLOAT;1;False;68;FLOAT2;0.8,1;False;47;FLOAT2;-0.05,0;False;29;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GradientNode;142;-432.8607,-220.2318;Inherit;False;0;3;2;0.1681589,0.1551264,0.3962264,0;0.1137255,0.4503766,0.8862745,0.6;0,0.6698114,0.8018868,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientNode;103;-418.765,-409.4424;Inherit;False;0;4;2;0.04685831,0.1520352,0.3679245,0;0.1112467,0.5721589,0.8204594,0.7942016;0.3760715,0.8649399,1,0.915938;0.4292453,0.8929623,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.GradientNode;141;-427.8607,-315.2318;Inherit;False;0;3;2;0.03137255,0.1574734,0.254902,0;0.1137255,0.5165758,0.8862745,0.4057984;0.6934233,0.4386792,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;115;22.99363,264.8655;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3.74;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;41;-428.3459,-19.51193;Inherit;True;Property;_Mask;Mask;21;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;35;-318.7549,1028.352;Inherit;True;Property;_MainTex;MainTex;6;0;Create;True;0;0;0;False;0;False;-1;1b58d66854585cb45a49a0048e0c0e95;1b58d66854585cb45a49a0048e0c0e95;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;96;-39.2452,1597.031;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;97.72671,1282.823;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;145;154.1295,27.06775;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;94;361.0695,1197.509;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;144;129.6389,-166.4041;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;119;37.56604,451.9738;Inherit;False;Property;_AColor0;AColor 0;12;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0,2.14859,16,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;156;-396.5228,674.5452;Inherit;False;RadialUVDistortion;-1;;4;051d65e7699b41a4c800363fd0e822b2;0;7;60;SAMPLER2D;_Sampler60156;False;1;FLOAT2;1,1;False;11;FLOAT2;0,0;False;65;FLOAT;1;False;68;FLOAT2;2,3;False;47;FLOAT2;-0.1,0;False;29;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GradientSampleNode;104;122.5594,-361.7219;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;114;256.3902,307.228;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;147;35.92897,662.5067;Inherit;True;Property;_TextureSample1;Texture Sample 1;18;0;Create;True;0;0;0;False;0;False;-1;None;802e94f7fa031bc4793bf60fbc8f17db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;106;578.3456,264.4612;Inherit;False;Property;_Bright;Bright;10;0;Create;True;0;0;0;False;0;False;0;3.28;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;143;541.629,-221.1446;Inherit;False;Property;_ColorSwitch;ColorSwitch;17;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;3;Key0;Key1;Key2;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;116;587.3064,1161.204;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;118;485.566,382.9738;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;56;916.3461,1052.875;Inherit;False;Property;_VertexOffset;VertexOffset;7;0;Create;True;0;0;0;False;0;False;0;0.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;113;449.6841,503.0012;Inherit;False;Property;_ABright;ABright;11;0;Create;True;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;942.3463,943.8746;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;112;670.9994,460.1818;Inherit;False;5;5;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;679.8372,147.5308;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;138;842.2064,597.5381;Inherit;False;Property;_Opacity;Opacity;16;0;Create;True;0;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;137;1050.18,624.5185;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;140;-615.8179,181.0215;Inherit;False;Constant;_Float0;Float 0;17;0;Create;True;0;0;0;False;0;False;7;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;157;-572.9471,697.4352;Inherit;False;Constant;_Float1;Float 1;18;0;Create;True;0;0;0;False;0;False;4.23;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;55;1155.329,814.9969;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;108;888.3871,289.347;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1283.669,157.6131;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;FBH/FBH_FX_portal2;False;False;False;False;True;True;True;True;True;False;True;True;False;False;True;False;False;False;False;False;False;Back;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;Transparent;All;5;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;2;10;25;True;0.5;False;2;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;5;-1;-1;0;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;130;0;128;0
WireConnection;129;0;126;0
WireConnection;129;2;130;0
WireConnection;135;0;134;0
WireConnection;125;0;129;0
WireConnection;125;1;127;0
WireConnection;162;0;161;0
WireConnection;131;0;125;0
WireConnection;131;3;135;0
WireConnection;131;4;134;0
WireConnection;84;0;83;0
WireConnection;133;0;131;0
WireConnection;164;0;162;0
WireConnection;89;0;85;0
WireConnection;160;0;164;0
WireConnection;160;1;84;0
WireConnection;87;0;86;0
WireConnection;87;2;89;0
WireConnection;124;0;87;0
WireConnection;124;1;136;0
WireConnection;81;0;82;0
WireConnection;81;2;160;0
WireConnection;100;0;99;0
WireConnection;88;1;124;0
WireConnection;123;0;81;0
WireConnection;123;1;136;0
WireConnection;98;0;97;0
WireConnection;98;2;100;0
WireConnection;115;0;139;0
WireConnection;35;1;123;0
WireConnection;96;0;98;0
WireConnection;101;0;88;4
WireConnection;101;1;102;0
WireConnection;145;0;142;0
WireConnection;145;1;41;1
WireConnection;94;0;35;4
WireConnection;94;1;101;0
WireConnection;94;2;96;0
WireConnection;144;0;141;0
WireConnection;144;1;41;1
WireConnection;104;0;103;0
WireConnection;104;1;41;1
WireConnection;114;0;115;0
WireConnection;147;1;156;0
WireConnection;143;1;104;0
WireConnection;143;0;144;0
WireConnection;143;2;145;0
WireConnection;116;0;94;0
WireConnection;118;0;114;0
WireConnection;118;1;119;0
WireConnection;54;0;116;0
WireConnection;54;1;56;0
WireConnection;112;0;118;0
WireConnection;112;1;113;0
WireConnection;112;2;88;4
WireConnection;112;3;41;1
WireConnection;112;4;147;4
WireConnection;105;0;143;0
WireConnection;105;1;106;0
WireConnection;137;0;138;0
WireConnection;137;1;116;0
WireConnection;55;1;54;0
WireConnection;108;0;105;0
WireConnection;108;1;112;0
WireConnection;0;2;108;0
WireConnection;0;9;137;0
WireConnection;0;11;55;0
ASEEND*/
//CHKSM=CBE0CCEAA4EF5F451CDA372A87D6ADCEAB71B16C