// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FBH/FBH_FX_portal4"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_RoSpeed("RoSpeed", Float) = 0
		_RoSpeed2("RoSpeed2", Float) = 0
		_Bright("Bright", Float) = 0
		_ABright("ABright", Float) = 10
		[HDR]_AColor0("AColor 0", Color) = (0,0,0,0)
		_NoiseScale("NoiseScale", Float) = 0
		_NoiseRoSpeed("NoiseRoSpeed", Float) = 0
		_NoiseSpeed("NoiseSpeed", Float) = 0
		[KeywordEnum(Key0,Key1,Key2)] _ColorSwitch("ColorSwitch", Float) = 0
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_2Str("2Str", Float) = 0
		_NoiseMu("NoiseMu", Float) = 0
		_Mask("Mask", 2D) = "white" {}
		[Enum(On,0,Off,1)]_ZWrite("ZWrite", Float) = 0
		_pnghut_vortexwhirlpoolspiralcircleblackandwhitesky("pnghut_vortex-whirlpool-spiral-circle-black-and-white-sky", 2D) = "white" {}
		_WarpTex("WarpTex", 2D) = "white" {}
		_TexWarp("TexWarp", Float) = 0
		_SmoothnessMax("SmoothnessMax", Range( 0 , 1)) = 0.5
		_SmoothnessMin("SmoothnessMin", Range( 0 , 1)) = 0.5
		_WarpTexBright("WarpTexBright", Float) = 0
		[HDR]_CentreColor0("CentreColor 0", Color) = (0,0,0,0)
		_WarpTexBrightHigh("WarpTexBrightHigh", Float) = 8
		_FlashSpeed("FlashSpeed", Float) = 0
		_Higtlight("Higtlight", Float) = 6
		_Contrast("Contrast", Float) = 0.8
		_RoSpeed3("RoSpeed3", Float) = -0.1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZWrite [_ZWrite]
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _COLORSWITCH_KEY0 _COLORSWITCH_KEY1 _COLORSWITCH_KEY2
		#pragma only_renderers d3d11 glcore gles gles3 metal 
		#pragma surface surf Unlit keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _ZWrite;
		uniform float4 _CentreColor0;
		uniform float _WarpTexBright;
		uniform sampler2D _WarpTex;
		uniform float _NoiseSpeed;
		uniform float _NoiseScale;
		uniform float _NoiseMu;
		uniform float _TexWarp;
		uniform float _RoSpeed2;
		uniform float _Contrast;
		uniform float _Higtlight;
		uniform float _WarpTexBrightHigh;
		uniform float _FlashSpeed;
		uniform sampler2D _Mask;
		uniform float4 _Mask_ST;
		uniform float _Bright;
		sampler2D _Sampler60139;
		uniform float4 _AColor0;
		uniform float _ABright;
		uniform sampler2D _MainTex;
		uniform sampler2D _TextureSample1;
		sampler2D _Sampler60156;
		uniform float _RoSpeed3;
		uniform float _SmoothnessMin;
		uniform float _SmoothnessMax;
		uniform sampler2D _pnghut_vortexwhirlpoolspiralcircleblackandwhitesky;
		uniform float _RoSpeed;
		uniform float _2Str;
		uniform float _NoiseRoSpeed;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		struct Gradient
		{
			int type;
			int colorsLength;
			int alphasLength;
			float4 colors[8];
			float2 alphas[8];
		};


		Gradient NewGradient(int type, int colorsLength, int alphasLength, 
		float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
		float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
		{
			Gradient g;
			g.type = type;
			g.colorsLength = colorsLength;
			g.alphasLength = alphasLength;
			g.colors[ 0 ] = colors0;
			g.colors[ 1 ] = colors1;
			g.colors[ 2 ] = colors2;
			g.colors[ 3 ] = colors3;
			g.colors[ 4 ] = colors4;
			g.colors[ 5 ] = colors5;
			g.colors[ 6 ] = colors6;
			g.colors[ 7 ] = colors7;
			g.alphas[ 0 ] = alphas0;
			g.alphas[ 1 ] = alphas1;
			g.alphas[ 2 ] = alphas2;
			g.alphas[ 3 ] = alphas3;
			g.alphas[ 4 ] = alphas4;
			g.alphas[ 5 ] = alphas5;
			g.alphas[ 6 ] = alphas6;
			g.alphas[ 7 ] = alphas7;
			return g;
		}


		float4 SampleGradient( Gradient gradient, float time )
		{
			float3 color = gradient.colors[0].rgb;
			UNITY_UNROLL
			for (int c = 1; c < 8; c++)
			{
			float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
			color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
			}
			#ifndef UNITY_COLORSPACE_GAMMA
			color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
			#endif
			float alpha = gradient.alphas[0].x;
			UNITY_UNROLL
			for (int a = 1; a < 8; a++)
			{
			float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
			alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
			}
			return float4(color, alpha);
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float mulTime130 = _Time.y * _NoiseSpeed;
			float cos129 = cos( mulTime130 );
			float sin129 = sin( mulTime130 );
			float2 rotator129 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos129 , -sin129 , sin129 , cos129 )) + float2( 0.5,0.5 );
			float simplePerlin2D125 = snoise( rotator129*_NoiseScale );
			simplePerlin2D125 = simplePerlin2D125*0.5 + 0.5;
			float DisNoise133 = (-_NoiseMu + (simplePerlin2D125 - 0.0) * (_NoiseMu - -_NoiseMu) / (1.0 - 0.0));
			float2 uv_TexCoord183 = i.uv_texcoord + float2( -0.5,-0.5 );
			float mulTime89 = _Time.y * _RoSpeed2;
			float Speed193 = mulTime89;
			float cos181 = cos( ( (0.0 + (length( uv_TexCoord183 ) - 0.0) * (_TexWarp - 0.0) / (1.0 - 0.0)) + Speed193 ) );
			float sin181 = sin( ( (0.0 + (length( uv_TexCoord183 ) - 0.0) * (_TexWarp - 0.0) / (1.0 - 0.0)) + Speed193 ) );
			float2 rotator181 = mul( ( i.uv_texcoord + DisNoise133 ) - float2( 0.5,0.5 ) , float2x2( cos181 , -sin181 , sin181 , cos181 )) + float2( 0.5,0.5 );
			float4 tex2DNode180 = tex2D( _WarpTex, rotator181 );
			float4 temp_cast_0 = (_Contrast).xxxx;
			float4 temp_cast_1 = (_Higtlight).xxxx;
			float mulTime208 = _Time.y * _FlashSpeed;
			float2 uv_TexCoord200 = i.uv_texcoord + float2( -0.5,-0.5 );
			float smoothstepResult202 = smoothstep( 0.03 , 0.1 , length( uv_TexCoord200 ));
			float4 lerpResult198 = lerp( _CentreColor0 , ( ( _WarpTexBright * pow( tex2DNode180 , temp_cast_0 ) ) + ( pow( tex2DNode180 , temp_cast_1 ) * _WarpTexBrightHigh * (0.2 + (sin( mulTime208 ) - -1.0) * (1.0 - 0.2) / (1.0 - -1.0)) ) ) , smoothstepResult202);
			Gradient gradient103 = NewGradient( 0, 3, 2, float4( 0.04685831, 0.1520352, 0.3679245, 0 ), float4( 0.1112467, 0.5721589, 0.8204594, 0.8666667 ), float4( 0.2017425, 0.6590272, 0.8817196, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float2 uv_Mask = i.uv_texcoord * _Mask_ST.xy + _Mask_ST.zw;
			float4 tex2DNode41 = tex2D( _Mask, uv_Mask );
			Gradient gradient141 = NewGradient( 0, 3, 2, float4( 0.1476059, 0.2289234, 0.2924528, 0 ), float4( 0.4810431, 0.693186, 0.8867924, 0.4753643 ), float4( 0.5887946, 0.4527386, 0.9794739, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			Gradient gradient142 = NewGradient( 0, 3, 2, float4( 0.2801228, 0.2736294, 0.4056604, 0 ), float4( 0.4741901, 0.6625477, 0.9056604, 0.6 ), float4( 0.3234247, 0.6954098, 0.8679245, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			#if defined(_COLORSWITCH_KEY0)
				float4 staticSwitch143 = SampleGradient( gradient103, tex2DNode41.r );
			#elif defined(_COLORSWITCH_KEY1)
				float4 staticSwitch143 = SampleGradient( gradient141, tex2DNode41.r );
			#elif defined(_COLORSWITCH_KEY2)
				float4 staticSwitch143 = SampleGradient( gradient142, tex2DNode41.r );
			#else
				float4 staticSwitch143 = SampleGradient( gradient103, tex2DNode41.r );
			#endif
			float2 temp_output_1_0_g2 = float2( 1,1 );
			float2 appendResult10_g2 = (float2(( (temp_output_1_0_g2).x * i.uv_texcoord.x ) , ( i.uv_texcoord.y * (temp_output_1_0_g2).y )));
			float2 temp_output_11_0_g2 = float2( 0,0 );
			float2 panner18_g2 = ( ( (temp_output_11_0_g2).x * _Time.y ) * float2( 1,0 ) + i.uv_texcoord);
			float2 panner19_g2 = ( ( _Time.y * (temp_output_11_0_g2).y ) * float2( 0,1 ) + i.uv_texcoord);
			float2 appendResult24_g2 = (float2((panner18_g2).x , (panner19_g2).y));
			float2 temp_output_47_0_g2 = float2( -0.05,0 );
			float2 uv_TexCoord78_g2 = i.uv_texcoord * float2( 2,2 );
			float2 temp_output_31_0_g2 = ( uv_TexCoord78_g2 - float2( 1,1 ) );
			float2 appendResult39_g2 = (float2(frac( ( atan2( (temp_output_31_0_g2).x , (temp_output_31_0_g2).y ) / 6.28318548202515 ) ) , length( temp_output_31_0_g2 )));
			float2 panner54_g2 = ( ( (temp_output_47_0_g2).x * _Time.y ) * float2( 1,0 ) + appendResult39_g2);
			float2 panner55_g2 = ( ( _Time.y * (temp_output_47_0_g2).y ) * float2( 0,1 ) + appendResult39_g2);
			float2 appendResult58_g2 = (float2((panner54_g2).x , (panner55_g2).y));
			float simplePerlin2D115 = snoise( ( ( (tex2D( _Sampler60139, ( appendResult10_g2 + appendResult24_g2 ) )).rg * 1.0 ) + ( float2( 0.7,5 ) * appendResult58_g2 ) )*3.74 );
			simplePerlin2D115 = simplePerlin2D115*0.5 + 0.5;
			float smoothstepResult114 = smoothstep( 0.75 , 1.0 , simplePerlin2D115);
			float cos87 = cos( mulTime89 );
			float sin87 = sin( mulTime89 );
			float2 rotator87 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos87 , -sin87 , sin87 , cos87 )) + float2( 0.5,0.5 );
			float4 tex2DNode88 = tex2D( _MainTex, ( rotator87 + DisNoise133 ) );
			float2 temp_output_1_0_g4 = float2( 1,1 );
			float2 appendResult10_g4 = (float2(( (temp_output_1_0_g4).x * i.uv_texcoord.x ) , ( i.uv_texcoord.y * (temp_output_1_0_g4).y )));
			float2 temp_output_11_0_g4 = float2( 0,0 );
			float2 panner18_g4 = ( ( (temp_output_11_0_g4).x * _Time.y ) * float2( 1,0 ) + i.uv_texcoord);
			float2 panner19_g4 = ( ( _Time.y * (temp_output_11_0_g4).y ) * float2( 0,1 ) + i.uv_texcoord);
			float2 appendResult24_g4 = (float2((panner18_g4).x , (panner19_g4).y));
			float2 appendResult220 = (float2(_RoSpeed3 , 0.0));
			float2 temp_output_47_0_g4 = appendResult220;
			float2 uv_TexCoord78_g4 = i.uv_texcoord * float2( 2,2 );
			float2 temp_output_31_0_g4 = ( uv_TexCoord78_g4 - float2( 1,1 ) );
			float2 appendResult39_g4 = (float2(frac( ( atan2( (temp_output_31_0_g4).x , (temp_output_31_0_g4).y ) / 6.28318548202515 ) ) , length( temp_output_31_0_g4 )));
			float2 panner54_g4 = ( ( (temp_output_47_0_g4).x * _Time.y ) * float2( 1,0 ) + appendResult39_g4);
			float2 panner55_g4 = ( ( _Time.y * (temp_output_47_0_g4).y ) * float2( 0,1 ) + appendResult39_g4);
			float2 appendResult58_g4 = (float2((panner54_g4).x , (panner55_g4).y));
			float2 uv_TexCoord167 = i.uv_texcoord + float2( -0.5,-0.5 );
			float smoothstepResult169 = smoothstep( _SmoothnessMin , _SmoothnessMax , length( uv_TexCoord167 ));
			float4 lerpResult165 = lerp( lerpResult198 , ( ( staticSwitch143 * _Bright ) + ( ( smoothstepResult114 * _AColor0 ) * _ABright * tex2DNode88.a * tex2DNode41.r * tex2D( _TextureSample1, ( ( (tex2D( _Sampler60156, ( appendResult10_g4 + appendResult24_g4 ) )).rg * 1.0 ) + ( float2( 2,3 ) * appendResult58_g4 ) ) ).a ) ) , smoothstepResult169);
			o.Emission = lerpResult165.rgb;
			float2 uv_TexCoord174 = i.uv_texcoord * float2( 0.7,0.7 ) + float2( 0.15,0.15 );
			float mulTime177 = _Time.y * -0.05;
			float cos176 = cos( mulTime177 );
			float sin176 = sin( mulTime177 );
			float2 rotator176 = mul( uv_TexCoord174 - float2( 0.5,0.51 ) , float2x2( cos176 , -sin176 , sin176 , cos176 )) + float2( 0.5,0.51 );
			float2 uv_TexCoord161 = i.uv_texcoord + float2( -0.5,-0.5 );
			float mulTime84 = _Time.y * _RoSpeed;
			float cos81 = cos( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float sin81 = sin( ( (0.0 + (length( uv_TexCoord161 ) - 0.0) * (5.0 - 0.0) / (1.0 - 0.0)) + mulTime84 ) );
			float2 rotator81 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos81 , -sin81 , sin81 , cos81 )) + float2( 0.5,0.5 );
			float mulTime100 = _Time.y * _NoiseRoSpeed;
			float cos98 = cos( mulTime100 );
			float sin98 = sin( mulTime100 );
			float2 rotator98 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos98 , -sin98 , sin98 , cos98 )) + float2( 0.5,0.5 );
			float simplePerlin2D96 = snoise( rotator98*3.0 );
			simplePerlin2D96 = simplePerlin2D96*0.5 + 0.5;
			float lerpResult94 = lerp( tex2D( _MainTex, ( rotator81 + DisNoise133 ) ).a , ( tex2DNode88.a * _2Str ) , simplePerlin2D96);
			float lerpResult166 = lerp( 1.0 , pow( lerpResult94 , 1.2 ) , smoothstepResult169);
			o.Alpha = saturate( ( ( (0.2 + (pow( tex2D( _pnghut_vortexwhirlpoolspiralcircleblackandwhitesky, rotator176 ).r , 3.0 ) - 0.0) * (1.0 - 0.2) / (1.0 - 0.0)) * lerpResult166 ) + ( 1.0 - smoothstepResult202 ) ) );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
49;50;1676;908;1197.557;-316.461;1.046869;True;False
Node;AmplifyShaderEditor.RangedFloatNode;128;-1215.399,2406.059;Inherit;False;Property;_NoiseSpeed;NoiseSpeed;9;0;Create;True;0;0;0;False;0;False;0;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;126;-1101.399,2245.059;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;130;-1035.653,2452.118;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;134;-692.6531,2659.117;Inherit;False;Property;_NoiseMu;NoiseMu;13;0;Create;True;0;0;0;False;0;False;0;0.005;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;129;-861.6531,2380.118;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;127;-844.3981,2536.059;Inherit;False;Property;_NoiseScale;NoiseScale;7;0;Create;True;0;0;0;False;0;False;0;12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;125;-659.4176,2408.167;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;135;-551.6531,2621.117;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;161;-1442.394,1173.426;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;85;-1191.396,1650.892;Inherit;False;Property;_RoSpeed2;RoSpeed2;3;0;Create;True;0;0;0;False;0;False;0;-0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-1167.928,1361.107;Inherit;False;Property;_RoSpeed;RoSpeed;2;0;Create;True;0;0;0;False;0;False;0;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;131;-431.6532,2462.118;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;162;-1206.692,1170.187;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;86;-1028.604,1457.547;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;164;-1065.692,1184.187;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;133;-240.6532,2492.118;Inherit;True;DisNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;183;1009.385,1587.646;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;89;-1033.396,1633.892;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;84;-1010.929,1364.107;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;193;-846.0314,1672.443;Inherit;False;Speed;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;184;1245.087,1584.407;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;-679.2313,1402.369;Inherit;False;133;DisNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;160;-858.6923,1249.187;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;87;-756.5109,1513.942;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;82;-1000.929,1060.108;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;186;1219.067,1773.665;Inherit;False;Property;_TexWarp;TexWarp;18;0;Create;True;0;0;0;False;0;False;0;14.58;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;99;-729.0621,1928.606;Inherit;False;Property;_NoiseRoSpeed;NoiseRoSpeed;8;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;97;-677.0621,1759.606;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;185;1386.088,1641.407;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;29.47;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;1454.711,1832.617;Inherit;False;193;Speed;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;100;-513.0665,1931.63;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;81;-715.8353,1179.502;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;124;-514.5146,1542.875;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;182;1227.86,1382.097;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;212;1288.63,1520.555;Inherit;False;133;DisNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;192;1654.174,1735.856;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;-367.9714,1494.575;Inherit;True;Property;_MainTex2;MainTex2;1;0;Create;True;0;0;0;False;0;False;-1;1b58d66854585cb45a49a0048e0c0e95;1b58d66854585cb45a49a0048e0c0e95;True;0;False;white;Auto;False;Instance;35;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;211;1497.93,1443.854;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;102;-213.062,1696.606;Inherit;False;Property;_2Str;2Str;12;0;Create;True;0;0;0;False;0;False;0;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;123;-472.8853,1304.357;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;174;795.3689,-63.59019;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;0.7,0.7;False;1;FLOAT2;0.15,0.15;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;177;937.3689,189.4098;Inherit;False;1;0;FLOAT;-0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;210;1898.47,1935.993;Inherit;False;Property;_FlashSpeed;FlashSpeed;24;0;Create;True;0;0;0;False;0;False;0;4.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;98;-292.1299,1825.582;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;83.84646,1486.4;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;139;-425.1848,234.4673;Inherit;False;RadialUVDistortion;-1;;2;051d65e7699b41a4c800363fd0e822b2;0;7;60;SAMPLER2D;_Sampler60139;False;1;FLOAT2;1,1;False;11;FLOAT2;0,0;False;65;FLOAT;1;False;68;FLOAT2;0.7,5;False;47;FLOAT2;-0.05,0;False;29;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;208;1958.47,1842.993;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;35;-332.6352,1231.929;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;0;False;0;False;-1;1b58d66854585cb45a49a0048e0c0e95;1b58d66854585cb45a49a0048e0c0e95;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;221;-682.4979,828.3798;Inherit;False;Property;_RoSpeed3;RoSpeed3;27;0;Create;True;0;0;0;False;0;False;-0.1;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;176;1013.173,72.40981;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.51;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;167;845.9309,438.8623;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;96;-53.12545,1800.608;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;181;1693.85,1468.789;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LengthOpNode;168;1062.931,443.8623;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;180;1870.752,1441.382;Inherit;True;Property;_WarpTex;WarpTex;17;0;Create;True;0;0;0;False;0;False;-1;None;242bcd8249e24b3488c22fb937263555;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;220;-520.2333,852.4578;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GradientNode;103;-418.765,-409.4424;Inherit;False;0;3;2;0.04685831,0.1520352,0.3679245,0;0.1112467,0.5721589,0.8204594,0.8666667;0.2017425,0.6590272,0.8817196,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.RangedFloatNode;187;865.3143,712.5446;Inherit;False;Property;_SmoothnessMax;SmoothnessMax;19;0;Create;True;0;0;0;False;0;False;0.5;0.478;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;200;2524.551,1833.784;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;94;347.1892,1401.086;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;171;1201.122,80.84387;Inherit;True;Property;_pnghut_vortexwhirlpoolspiralcircleblackandwhitesky;pnghut_vortex-whirlpool-spiral-circle-black-and-white-sky;16;0;Create;True;0;0;0;False;0;False;-1;7a1a06bf8c3921142bbcfeb49c41aed6;7a1a06bf8c3921142bbcfeb49c41aed6;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;218;2067.473,1641.108;Inherit;False;Property;_Higtlight;Higtlight;25;0;Create;True;0;0;0;False;0;False;6;13.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;219;2050.473,1345.108;Inherit;False;Property;_Contrast;Contrast;26;0;Create;True;0;0;0;False;0;False;0.8;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GradientNode;142;-432.8607,-220.2318;Inherit;False;0;3;2;0.2801228,0.2736294,0.4056604,0;0.4741901,0.6625477,0.9056604,0.6;0.3234247,0.6954098,0.8679245,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SamplerNode;41;-428.3459,-19.51193;Inherit;True;Property;_Mask;Mask;14;0;Create;True;0;0;0;False;0;False;-1;None;0000000000000000f000000000000000;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientNode;141;-427.8607,-315.2318;Inherit;False;0;3;2;0.1476059,0.2289234,0.2924528,0;0.4810431,0.693186,0.8867924,0.4753643;0.5887946,0.4527386,0.9794739,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;867.488,564.3556;Inherit;False;Property;_SmoothnessMin;SmoothnessMin;20;0;Create;True;0;0;0;False;0;False;0.5;0.1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;115;16.82494,286.456;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3.74;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;207;2136.47,1832.993;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;206;2036.47,1985.993;Inherit;False;Property;_WarpTexBrightHigh;WarpTexBrightHigh;23;0;Create;True;0;0;0;False;0;False;8;200;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;201;2734.652,1755.146;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;114;256.3902,307.228;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;119;37.56604,451.9738;Inherit;False;Property;_AColor0;AColor 0;6;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;10.85376,1.756862,23.96863,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;209;2287.47,1817.993;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0.2;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;214;2237.452,1389.41;Inherit;False;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.8;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;196;2159.885,1217.019;Inherit;False;Property;_WarpTexBright;WarpTexBright;21;0;Create;True;0;0;0;False;0;False;0;1.83;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;145;154.1295,27.06775;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;144;129.6389,-166.4041;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;104;122.5594,-361.7219;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;203;2228.628,1506.092;Inherit;False;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;6;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;178;1594.521,133.1009;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;116;573.4261,1364.781;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;156;-396.5228,674.5452;Inherit;False;RadialUVDistortion;-1;;4;051d65e7699b41a4c800363fd0e822b2;0;7;60;SAMPLER2D;_Sampler60156;False;1;FLOAT2;1,1;False;11;FLOAT2;0,0;False;65;FLOAT;1;False;68;FLOAT2;2,3;False;47;FLOAT2;-0.1,0;False;29;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;169;1203.931,486.8623;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.1;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;147;35.92897,662.5067;Inherit;True;Property;_TextureSample1;Texture Sample 1;11;0;Create;True;0;0;0;False;0;False;-1;None;802e94f7fa031bc4793bf60fbc8f17db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;175;1767.599,169.5219;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.2;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;118;485.566,382.9738;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;202;2892.559,1723.529;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.03;False;2;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;143;541.629,-221.1446;Inherit;False;Property;_ColorSwitch;ColorSwitch;10;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;3;Key0;Key1;Key2;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;195;2386.347,1331.965;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;113;449.6841,503.0012;Inherit;False;Property;_ABright;ABright;5;0;Create;True;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;166;1549.569,564.4273;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;205;2467.628,1576.092;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;106;578.3456,264.4612;Inherit;False;Property;_Bright;Bright;4;0;Create;True;0;0;0;False;0;False;0;3.28;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;204;2496.628,1463.092;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;112;670.9994,460.1818;Inherit;False;5;5;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;679.8372,147.5308;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;179;1875.103,499.1565;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;215;1792.737,711.1329;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;199;2621.546,1545.716;Inherit;False;Property;_CentreColor0;CentreColor 0;22;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;2,1.040698,0.8962264,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;216;2030.664,562.2217;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;108;888.3871,289.347;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;198;3087.777,1500.609;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;140;-615.8179,181.0215;Inherit;False;Constant;_Float0;Float 0;17;0;Create;True;0;0;0;False;0;False;7;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;165;1587.55,384.742;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;217;2180.56,724.1093;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;170;2493.054,376.9766;Inherit;False;Property;_ZWrite;ZWrite;15;1;[Enum];Create;True;0;2;On;0;Off;1;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2199.269,305.0132;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;FBH/FBH_FX_portal4;False;False;False;False;True;True;True;True;True;False;True;True;False;False;True;False;False;False;False;False;False;Back;0;True;170;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;Transparent;All;5;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;2;10;25;True;0.5;False;2;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;130;0;128;0
WireConnection;129;0;126;0
WireConnection;129;2;130;0
WireConnection;125;0;129;0
WireConnection;125;1;127;0
WireConnection;135;0;134;0
WireConnection;131;0;125;0
WireConnection;131;3;135;0
WireConnection;131;4;134;0
WireConnection;162;0;161;0
WireConnection;164;0;162;0
WireConnection;133;0;131;0
WireConnection;89;0;85;0
WireConnection;84;0;83;0
WireConnection;193;0;89;0
WireConnection;184;0;183;0
WireConnection;160;0;164;0
WireConnection;160;1;84;0
WireConnection;87;0;86;0
WireConnection;87;2;89;0
WireConnection;185;0;184;0
WireConnection;185;4;186;0
WireConnection;100;0;99;0
WireConnection;81;0;82;0
WireConnection;81;2;160;0
WireConnection;124;0;87;0
WireConnection;124;1;136;0
WireConnection;192;0;185;0
WireConnection;192;1;194;0
WireConnection;88;1;124;0
WireConnection;211;0;182;0
WireConnection;211;1;212;0
WireConnection;123;0;81;0
WireConnection;123;1;136;0
WireConnection;98;0;97;0
WireConnection;98;2;100;0
WireConnection;101;0;88;4
WireConnection;101;1;102;0
WireConnection;208;0;210;0
WireConnection;35;1;123;0
WireConnection;176;0;174;0
WireConnection;176;2;177;0
WireConnection;96;0;98;0
WireConnection;181;0;211;0
WireConnection;181;2;192;0
WireConnection;168;0;167;0
WireConnection;180;1;181;0
WireConnection;220;0;221;0
WireConnection;94;0;35;4
WireConnection;94;1;101;0
WireConnection;94;2;96;0
WireConnection;171;1;176;0
WireConnection;115;0;139;0
WireConnection;207;0;208;0
WireConnection;201;0;200;0
WireConnection;114;0;115;0
WireConnection;209;0;207;0
WireConnection;214;0;180;0
WireConnection;214;1;219;0
WireConnection;145;0;142;0
WireConnection;145;1;41;1
WireConnection;144;0;141;0
WireConnection;144;1;41;1
WireConnection;104;0;103;0
WireConnection;104;1;41;1
WireConnection;203;0;180;0
WireConnection;203;1;218;0
WireConnection;178;0;171;1
WireConnection;116;0;94;0
WireConnection;156;47;220;0
WireConnection;169;0;168;0
WireConnection;169;1;197;0
WireConnection;169;2;187;0
WireConnection;147;1;156;0
WireConnection;175;0;178;0
WireConnection;118;0;114;0
WireConnection;118;1;119;0
WireConnection;202;0;201;0
WireConnection;143;1;104;0
WireConnection;143;0;144;0
WireConnection;143;2;145;0
WireConnection;195;0;196;0
WireConnection;195;1;214;0
WireConnection;166;1;116;0
WireConnection;166;2;169;0
WireConnection;205;0;203;0
WireConnection;205;1;206;0
WireConnection;205;2;209;0
WireConnection;204;0;195;0
WireConnection;204;1;205;0
WireConnection;112;0;118;0
WireConnection;112;1;113;0
WireConnection;112;2;88;4
WireConnection;112;3;41;1
WireConnection;112;4;147;4
WireConnection;105;0;143;0
WireConnection;105;1;106;0
WireConnection;179;0;175;0
WireConnection;179;1;166;0
WireConnection;215;0;202;0
WireConnection;216;0;179;0
WireConnection;216;1;215;0
WireConnection;108;0;105;0
WireConnection;108;1;112;0
WireConnection;198;0;199;0
WireConnection;198;1;204;0
WireConnection;198;2;202;0
WireConnection;165;0;198;0
WireConnection;165;1;108;0
WireConnection;165;2;169;0
WireConnection;217;0;216;0
WireConnection;0;2;165;0
WireConnection;0;9;217;0
ASEEND*/
//CHKSM=E3EFD2FE02005C0EB136EBD34AAEABBD97E8A602