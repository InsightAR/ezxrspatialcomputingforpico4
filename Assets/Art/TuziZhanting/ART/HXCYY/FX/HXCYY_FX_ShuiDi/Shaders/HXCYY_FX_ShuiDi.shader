// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "HXCYY/HXCYY_FX_ShuiDi"
{
	Properties
	{
		[HDR][SingleLineTexture]_CubeMap("CubeMap", CUBE) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[HDR]_Color0("Color 0", Color) = (0,0,0,0)
		_Switch("Switch", Range( 0 , 1)) = 0
		_NoiseScale("NoiseScale", Float) = 0
		_Speed("Speed", Float) = 0
		_Lum("Lum", Float) = 0
		[HDR]_FresnelColor("FresnelColor", Color) = (0,0,0,0)
		_FresnelBSP("FresnelBSP", Vector) = (0.15,0.83,1.42,0)
		_FresnelBSPIdle("FresnelBSPIdle", Vector) = (0.15,0.83,1.42,0)
		_BreathSpeed("BreathSpeed", Float) = 0
		_BreathingSwitch("BreathingSwitch", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float3 worldRefl;
			INTERNAL_DATA
			float2 uv_texcoord;
			float3 worldPos;
			float3 viewDir;
			float3 worldNormal;
		};

		uniform float _Lum;
		uniform samplerCUBE _CubeMap;
		uniform float4 _Color0;
		uniform float _Speed;
		uniform float _NoiseScale;
		uniform float _Switch;
		uniform float3 _FresnelBSP;
		uniform float3 _FresnelBSPIdle;
		uniform float _BreathSpeed;
		uniform float _BreathingSwitch;
		uniform float4 _FresnelColor;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldReflection = i.worldRefl;
			float2 uv_TexCoord16 = i.uv_texcoord * float2( 2,0.5 ) + float2( 0,0.28 );
			float2 temp_output_53_0_g18 = ( frac( ( uv_TexCoord16 / float2( 2,2 ) ) ) * float2( 2,2 ) );
			float2 temp_output_31_0_g18 = step( float2( 1,1 ) , temp_output_53_0_g18 );
			float2 temp_output_30_0_g18 = step( temp_output_53_0_g18 , float2( 0,0 ) );
			float temp_output_46_0_g18 = 0.0;
			float2 temp_cast_0 = (temp_output_46_0_g18).xx;
			float2 temp_cast_1 = (( 1.0 - temp_output_46_0_g18 )).xx;
			float2 clampResult44_g18 = clamp( ( ( temp_output_31_0_g18 * ( ( 1.0 - ( temp_output_31_0_g18 * temp_output_53_0_g18 ) ) + float2( 1,1 ) ) ) + ( ( 1.0 - temp_output_31_0_g18 ) * temp_output_53_0_g18 * ( 1.0 - temp_output_30_0_g18 ) ) + ( temp_output_30_0_g18 * ( ( 1.0 - ( temp_output_53_0_g18 * temp_output_30_0_g18 ) ) - float2( 1,1 ) ) ) ) , temp_cast_0 , temp_cast_1 );
			float mulTime49 = _Time.y * _Speed;
			float2 temp_cast_2 = (mulTime49).xx;
			float2 uv_TexCoord46 = i.uv_texcoord * float2( 2,0.5 ) + temp_cast_2;
			float2 temp_output_53_0_g17 = ( frac( ( uv_TexCoord46 / float2( 2,2 ) ) ) * float2( 2,2 ) );
			float2 temp_output_31_0_g17 = step( float2( 1,1 ) , temp_output_53_0_g17 );
			float2 temp_output_30_0_g17 = step( temp_output_53_0_g17 , float2( 0,0 ) );
			float temp_output_46_0_g17 = 0.0;
			float2 temp_cast_3 = (temp_output_46_0_g17).xx;
			float2 temp_cast_4 = (( 1.0 - temp_output_46_0_g17 )).xx;
			float2 clampResult44_g17 = clamp( ( ( temp_output_31_0_g17 * ( ( 1.0 - ( temp_output_31_0_g17 * temp_output_53_0_g17 ) ) + float2( 1,1 ) ) ) + ( ( 1.0 - temp_output_31_0_g17 ) * temp_output_53_0_g17 * ( 1.0 - temp_output_30_0_g17 ) ) + ( temp_output_30_0_g17 * ( ( 1.0 - ( temp_output_53_0_g17 * temp_output_30_0_g17 ) ) - float2( 1,1 ) ) ) ) , temp_cast_3 , temp_cast_4 );
			float simplePerlin2D45 = snoise( clampResult44_g17*0.2 );
			simplePerlin2D45 = simplePerlin2D45*0.5 + 0.5;
			float simplePerlin2D13 = snoise( ( clampResult44_g18 + (-0.2 + (simplePerlin2D45 - 0.0) * (0.2 - -0.2) / (1.0 - 0.0)) )*_NoiseScale );
			simplePerlin2D13 = simplePerlin2D13*0.5 + 0.5;
			float temp_output_50_0 = step( simplePerlin2D13 , ( _Switch + 0.03 ) );
			float3 ase_worldNormal = i.worldNormal;
			float mulTime68 = _Time.y * _BreathSpeed;
			float3 lerpResult64 = lerp( _FresnelBSP , _FresnelBSPIdle , (0.0 + (sin( mulTime68 ) - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)));
			float3 lerpResult70 = lerp( _FresnelBSP , lerpResult64 , _BreathingSwitch);
			float3 break65 = lerpResult70;
			float fresnelNdotV56 = dot( ase_worldNormal, i.viewDir );
			float fresnelNode56 = ( break65.x + break65.y * pow( 1.0 - fresnelNdotV56, break65.z ) );
			o.Emission = ( ( _Lum * texCUBE( _CubeMap, ase_worldReflection ) ) + ( _Color0 * ( temp_output_50_0 - step( simplePerlin2D13 , _Switch ) ) ) + ( saturate( fresnelNode56 ) * _FresnelColor ) ).rgb;
			o.Alpha = 1;
			float temp_output_62_0 = saturate( temp_output_50_0 );
			clip( temp_output_62_0 - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "LWGUI.LWGUI"
}
/*ASEBEGIN
Version=18935
49;50;1676;908;844.5944;950.5574;1.221225;True;False
Node;AmplifyShaderEditor.RangedFloatNode;48;-1963.873,751.6739;Inherit;False;Property;_Speed;Speed;5;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;49;-1807.873,744.6739;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;69;-1623.736,-415.6609;Inherit;False;Property;_BreathSpeed;BreathSpeed;10;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;46;-1636.873,624.6739;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,0.5;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;68;-1414.907,-404.6699;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;43;-1334.572,624.4657;Inherit;False;JingXiang 镜像;-1;;17;c86680d302773f44690401019249c4c3;0;2;21;FLOAT2;0,0;False;46;FLOAT;0;False;3;FLOAT2;0;FLOAT;58;FLOAT;57
Node;AmplifyShaderEditor.SinOpNode;66;-1213.405,-422.9884;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;59;-1092.753,-804.1354;Inherit;False;Property;_FresnelBSP;FresnelBSP;8;0;Create;True;0;0;0;False;0;False;0.15,0.83,1.42;0,1,5;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector3Node;63;-1104.31,-631.7124;Inherit;False;Property;_FresnelBSPIdle;FresnelBSPIdle;9;0;Create;True;0;0;0;False;0;False;0.15,0.83,1.42;0,1,5;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TFHCRemapNode;67;-1050.982,-429.0945;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;45;-1119.873,622.6739;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;16;-1265.912,416.7511;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,0.5;False;1;FLOAT2;0,0.28;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;47;-890.8734,655.6739;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.2;False;4;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;64;-826.2761,-576.8628;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-690.7201,-370.4756;Inherit;False;Property;_BreathingSwitch;BreathingSwitch;11;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;41;-1003.31,447.3346;Inherit;False;JingXiang 镜像;-1;;18;c86680d302773f44690401019249c4c3;0;2;21;FLOAT2;0,0;False;46;FLOAT;0;False;3;FLOAT2;0;FLOAT;58;FLOAT;57
Node;AmplifyShaderEditor.SimpleAddOpNode;42;-728.3096,512.3346;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-571.912,599.7511;Inherit;False;Property;_NoiseScale;NoiseScale;4;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-667.912,356.7511;Inherit;False;Property;_Switch;Switch;3;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;70;-472.1208,-568.3141;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;58;-317.7388,-823.9805;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;65;-260.8489,-589.075;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.WorldNormalVector;57;-272.5243,-985.858;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-377.0221,528.3396;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.03;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;13;-573.912,479.7511;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldReflectionVector;11;-905.7487,51.80518;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StepOpNode;50;-240.0221,490.3396;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;56;-64.52448,-759.6581;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;14;-358.912,370.7511;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-580,38;Inherit;True;Property;_CubeMap;CubeMap;0;2;[HDR];[SingleLineTexture];Create;True;0;0;0;False;0;False;-1;dc585d4ac4942c64389c7a882267b8dc;dc585d4ac4942c64389c7a882267b8dc;True;0;False;white;LockedToCube;False;Object;-1;Auto;Cube;8;0;SAMPLERCUBE;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;55;-434.0221,-81.6604;Inherit;False;Property;_Lum;Lum;6;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;3;-255.7,123.5;Inherit;False;Property;_Color0;Color 0;2;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;52;-154.0221,364.3396;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;61;23.19084,-148.3885;Inherit;False;Property;_FresnelColor;FresnelColor;7;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;72;297.251,-524.3499;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-40.50001,19.99999;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;72.87788,167.2396;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;294.8908,-189.9885;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;362.8778,30.33963;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;62;535.3908,352.1114;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;763.3998,85.2;Float;False;True;-1;2;LWGUI.LWGUI;0;0;Unlit;HXCYY/HXCYY_FX_ShuiDi;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;False;0;False;TransparentCutout;;AlphaTest;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;49;0;48;0
WireConnection;46;1;49;0
WireConnection;68;0;69;0
WireConnection;43;21;46;0
WireConnection;66;0;68;0
WireConnection;67;0;66;0
WireConnection;45;0;43;0
WireConnection;47;0;45;0
WireConnection;64;0;59;0
WireConnection;64;1;63;0
WireConnection;64;2;67;0
WireConnection;41;21;16;0
WireConnection;42;0;41;0
WireConnection;42;1;47;0
WireConnection;70;0;59;0
WireConnection;70;1;64;0
WireConnection;70;2;71;0
WireConnection;65;0;70;0
WireConnection;51;0;15;0
WireConnection;13;0;42;0
WireConnection;13;1;17;0
WireConnection;50;0;13;0
WireConnection;50;1;51;0
WireConnection;56;0;57;0
WireConnection;56;4;58;0
WireConnection;56;1;65;0
WireConnection;56;2;65;1
WireConnection;56;3;65;2
WireConnection;14;0;13;0
WireConnection;14;1;15;0
WireConnection;1;1;11;0
WireConnection;52;0;50;0
WireConnection;52;1;14;0
WireConnection;72;0;56;0
WireConnection;2;0;55;0
WireConnection;2;1;1;0
WireConnection;54;0;3;0
WireConnection;54;1;52;0
WireConnection;60;0;72;0
WireConnection;60;1;61;0
WireConnection;53;0;2;0
WireConnection;53;1;54;0
WireConnection;53;2;60;0
WireConnection;62;0;50;0
WireConnection;0;2;53;0
WireConnection;0;9;62;0
WireConnection;0;10;62;0
ASEEND*/
//CHKSM=FEF33FB71BB4236E4D26B5C8096C37CFD62ED464