// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "LTZT/LTZT_FX_floor"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_MainTex("MainTex", 2D) = "white" {}
		_Count("Count", Float) = 0
		[HDR]_Square("Square", Color) = (0,0,0,0)
		_SquareSize("SquareSize", Float) = 0
		_SquareWidth("SquareWidth", Float) = 0
		[HDR]_Line("Line", Color) = (0,0,0,0)
		_Speed("Speed", Float) = 0
		_NoiseScale("NoiseScale", Float) = 0
		_PointSize("PointSize", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma only_renderers d3d11 glcore gles gles3 metal 
		#pragma surface surf Unlit keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Square;
		uniform float _Count;
		uniform float _SquareSize;
		uniform float _SquareWidth;
		uniform float _PointSize;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _Speed;
		uniform float _NoiseScale;
		uniform float4 _Line;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float3 temp_output_11_0 = ( _Count * ase_objectScale );
			float2 uv_TexCoord5 = i.uv_texcoord * temp_output_11_0.xy + ( 1.0 - temp_output_11_0 ).xy;
			float2 temp_output_6_0 = frac( uv_TexCoord5 );
			float temp_output_15_0 = ( _SquareSize + _SquareWidth );
			float temp_output_2_0_g2 = 4.0;
			float cosSides12_g2 = cos( ( UNITY_PI / temp_output_2_0_g2 ) );
			float2 appendResult18_g2 = (float2(( temp_output_15_0 * cosSides12_g2 ) , ( temp_output_15_0 * cosSides12_g2 )));
			float2 break23_g2 = ( (temp_output_6_0*2.0 + -1.0) / appendResult18_g2 );
			float polarCoords30_g2 = atan2( break23_g2.x , -break23_g2.y );
			float temp_output_52_0_g2 = ( 6.28318548202515 / temp_output_2_0_g2 );
			float2 appendResult25_g2 = (float2(break23_g2.x , -break23_g2.y));
			float2 finalUVs29_g2 = appendResult25_g2;
			float temp_output_44_0_g2 = ( cos( ( ( floor( ( 0.5 + ( polarCoords30_g2 / temp_output_52_0_g2 ) ) ) * temp_output_52_0_g2 ) - polarCoords30_g2 ) ) * length( finalUVs29_g2 ) );
			float temp_output_2_0_g3 = 4.0;
			float cosSides12_g3 = cos( ( UNITY_PI / temp_output_2_0_g3 ) );
			float2 appendResult18_g3 = (float2(( _SquareSize * cosSides12_g3 ) , ( _SquareSize * cosSides12_g3 )));
			float2 break23_g3 = ( (temp_output_6_0*2.0 + -1.0) / appendResult18_g3 );
			float polarCoords30_g3 = atan2( break23_g3.x , -break23_g3.y );
			float temp_output_52_0_g3 = ( 6.28318548202515 / temp_output_2_0_g3 );
			float2 appendResult25_g3 = (float2(break23_g3.x , -break23_g3.y));
			float2 finalUVs29_g3 = appendResult25_g3;
			float temp_output_44_0_g3 = ( cos( ( ( floor( ( 0.5 + ( polarCoords30_g3 / temp_output_52_0_g3 ) ) ) * temp_output_52_0_g3 ) - polarCoords30_g3 ) ) * length( finalUVs29_g3 ) );
			float temp_output_2_0_g4 = 8.0;
			float cosSides12_g4 = cos( ( UNITY_PI / temp_output_2_0_g4 ) );
			float2 appendResult18_g4 = (float2(( _PointSize * cosSides12_g4 ) , ( _PointSize * cosSides12_g4 )));
			float2 break23_g4 = ( (frac( ( uv_TexCoord5 + float2( 0.5,0.5 ) ) )*2.0 + -1.0) / appendResult18_g4 );
			float polarCoords30_g4 = atan2( break23_g4.x , -break23_g4.y );
			float temp_output_52_0_g4 = ( 6.28318548202515 / temp_output_2_0_g4 );
			float2 appendResult25_g4 = (float2(break23_g4.x , -break23_g4.y));
			float2 finalUVs29_g4 = appendResult25_g4;
			float temp_output_44_0_g4 = ( cos( ( ( floor( ( 0.5 + ( polarCoords30_g4 / temp_output_52_0_g4 ) ) ) * temp_output_52_0_g4 ) - polarCoords30_g4 ) ) * length( finalUVs29_g4 ) );
			float temp_output_45_0 = ( ( saturate( ( ( 1.0 - temp_output_44_0_g2 ) / fwidth( temp_output_44_0_g2 ) ) ) - saturate( ( ( 1.0 - temp_output_44_0_g3 ) / fwidth( temp_output_44_0_g3 ) ) ) ) + saturate( ( ( 1.0 - temp_output_44_0_g4 ) / fwidth( temp_output_44_0_g4 ) ) ) );
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float mulTime37 = _Time.y * _Speed;
			float cos41 = cos( mulTime37 );
			float sin41 = sin( mulTime37 );
			float2 rotator41 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos41 , -sin41 , sin41 , cos41 )) + float2( 0.5,0.5 );
			float simplePerlin2D34 = snoise( rotator41*_NoiseScale );
			simplePerlin2D34 = simplePerlin2D34*0.5 + 0.5;
			float smoothstepResult40 = smoothstep( 0.3 , 1.0 , simplePerlin2D34);
			float temp_output_33_0 = ( tex2D( _MainTex, uv_MainTex ).r * smoothstepResult40 );
			o.Emission = ( ( _Square * temp_output_45_0 ) + ( temp_output_33_0 * _Line ) ).rgb;
			o.Alpha = 1;
			clip( saturate( ( temp_output_45_0 + temp_output_33_0 ) ) - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
144;191;1646;745;4302.519;1264.419;4.043411;True;False
Node;AmplifyShaderEditor.ObjectScaleNode;10;-1418.696,282.1752;Inherit;False;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;12;-1368.696,165.1752;Inherit;False;Property;_Count;Count;2;0;Create;True;0;0;0;False;0;False;0;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-1229.696,243.1752;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-1406.206,1263.497;Inherit;False;Property;_Speed;Speed;7;0;Create;True;0;0;0;False;0;False;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;22;-1100.101,322.8022;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-792.6963,406.1752;Inherit;False;Property;_SquareSize;SquareSize;4;0;Create;True;0;0;0;False;0;False;0;1.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-954,192.5;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-784.6963,524.1752;Inherit;False;Property;_SquareWidth;SquareWidth;5;0;Create;True;0;0;0;False;0;False;0;0.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;37;-1240.498,1315.576;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;35;-1065.321,1190.901;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;13;-775.6963,310.1752;Inherit;False;Constant;_SquareSide;SquareSide;3;0;Create;True;0;0;0;False;0;False;4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;41;-771.3353,1227.44;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-643.6963,464.1752;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;6;-670.6963,196.1752;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-631.245,1337.962;Inherit;False;Property;_NoiseScale;NoiseScale;8;0;Create;True;0;0;0;False;0;False;0;2.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;46;-871.2799,620.8506;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;4;-519,201.5;Inherit;False;Polygon;-1;;3;6906ef7087298c94c853d6753e182169;0;4;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;48;-714.088,628.5932;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;34;-471.6637,1216.708;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-694.4071,693.8163;Inherit;False;Constant;_PointSide;PointSide;9;0;Create;True;0;0;0;False;0;False;8;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-694.6072,795.6163;Inherit;False;Property;_PointSize;PointSize;9;0;Create;True;0;0;0;False;0;False;0;0.13;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;7;-520.6963,384.1752;Inherit;False;Polygon;-1;;2;6906ef7087298c94c853d6753e182169;0;4;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;17;-311.2963,296.5753;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-724.9191,936.8707;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;0;False;0;False;-1;None;1cc007f8b9709d24c93d6c0165dddfab;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;42;-514.6317,636.9594;Inherit;True;Polygon;-1;;4;6906ef7087298c94c853d6753e182169;0;4;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;40;-165.2356,1171.64;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.3;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-184.1522,727.1472;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;3;-562,-175;Inherit;False;Property;_Square;Square;3;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0.6084906,0.7077577,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;45;-162.1442,411.5047;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;26;-424.4691,6.743781;Inherit;False;Property;_Line;Line;6;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0.7697256,1.761395,3.980045,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;19;13.24966,589.9731;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-67.80108,103.8022;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-85.49999,-172.4;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;20;65.97758,419.7332;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25;77.33081,73.04376;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;266.5,83.2;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;LTZT/LTZT_FX_floor;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;False;0;False;TransparentCutout;;AlphaTest;All;5;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;11;0;12;0
WireConnection;11;1;10;0
WireConnection;22;0;11;0
WireConnection;5;0;11;0
WireConnection;5;1;22;0
WireConnection;37;0;36;0
WireConnection;41;0;35;0
WireConnection;41;2;37;0
WireConnection;15;0;14;0
WireConnection;15;1;16;0
WireConnection;6;0;5;0
WireConnection;46;0;5;0
WireConnection;4;1;6;0
WireConnection;4;2;13;0
WireConnection;4;3;14;0
WireConnection;4;4;14;0
WireConnection;48;0;46;0
WireConnection;34;0;41;0
WireConnection;34;1;38;0
WireConnection;7;1;6;0
WireConnection;7;2;13;0
WireConnection;7;3;15;0
WireConnection;7;4;15;0
WireConnection;17;0;7;0
WireConnection;17;1;4;0
WireConnection;42;1;48;0
WireConnection;42;2;43;0
WireConnection;42;3;44;0
WireConnection;42;4;44;0
WireConnection;40;0;34;0
WireConnection;33;0;1;1
WireConnection;33;1;40;0
WireConnection;45;0;17;0
WireConnection;45;1;42;0
WireConnection;19;0;45;0
WireConnection;19;1;33;0
WireConnection;24;0;33;0
WireConnection;24;1;26;0
WireConnection;2;0;3;0
WireConnection;2;1;45;0
WireConnection;20;0;19;0
WireConnection;25;0;2;0
WireConnection;25;1;24;0
WireConnection;0;2;25;0
WireConnection;0;10;20;0
ASEEND*/
//CHKSM=FE093983244D8964CAFFC42AD1EFB0CFF48432AD