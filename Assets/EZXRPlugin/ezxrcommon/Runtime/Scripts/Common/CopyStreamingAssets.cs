using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
namespace EzxrCore.Common
{
public class CopyStreamingAssets : MonoBehaviour
{
    public delegate void CopyCompletedDelegate(string assetPath);
    public event CopyCompletedDelegate OnCopyCompleted;

    public string assetsDirectory_ios = "";
    public string assetsDirectory_android = "";
    public List<string> filesToCopy_android = new List<string>() { "", "", ""};
    
    [SerializeField]
    Text m_CopyAssetInfoText;

    /// <summary>
    /// The UI Text used to display information about the availability of depth functionality.
    /// </summary>
    public Text copyAssetInfoText
    {
        get => m_CopyAssetInfoText;
        set => m_CopyAssetInfoText = value;
    }

    private bool isCopyFinished = false;
    IEnumerator Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if(m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = true;
            yield return CopyStreamingAssetsAndroid();
            if (isCopyFinished)
            {
                if(m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = false;
                string destinationPath = Path.Combine(Application.persistentDataPath, assetsDirectory_android);
                OnCopyCompleted?.Invoke(destinationPath);
            }
            else
            {
                OnCopyCompleted?.Invoke(null);
            }
        }
        else
        {
            yield return null;
            if(m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = false;
            string assetPath = Path.Combine(Application.streamingAssetsPath, assetsDirectory_ios);
            if (Directory.Exists(assetPath))
            {
                isCopyFinished = true;
                OnCopyCompleted?.Invoke(assetPath);
            }
            else
            {
                OnCopyCompleted?.Invoke(null);
            }
        }
    }

    IEnumerator CopyStreamingAssetsAndroid()
    {
        string sourcePath = Path.Combine(Application.streamingAssetsPath, assetsDirectory_android);
        string destinationPath = Path.Combine(Application.persistentDataPath, assetsDirectory_android);
        
        if (!Directory.Exists(destinationPath))
        {
            Directory.CreateDirectory(destinationPath);
        }
        
        int copyFailedCount = 0;
        foreach (string fileName in filesToCopy_android)
        {
            string sourceFilePath = Path.Combine(sourcePath, fileName); ;
            string destinationFilePath = Path.Combine(destinationPath, fileName);
            //create directory
            string directoryPath = Path.GetDirectoryName(destinationFilePath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (!File.Exists(destinationFilePath))
            {
                Debug.Log("CopyStreamingAssetsAndroid: " + sourceFilePath);
                using (UnityWebRequest www = UnityWebRequest.Get(sourceFilePath))
                {
                    yield return www.SendWebRequest();

                    if (www.result == UnityWebRequest.Result.Success)
                    {
                        File.WriteAllBytes(destinationFilePath, www.downloadHandler.data);
                    }
                    else
                    {
                        Debug.LogError("Failed to load streaming assets: " + www.error);
                        copyFailedCount++;
                    }
                }
            }
        }
        if (copyFailedCount == 0)
        {
            isCopyFinished = true;
        }
    }
}
}
