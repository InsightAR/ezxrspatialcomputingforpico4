using System.Collections;
using System.Collections.Generic;
using EzxrCore.Log;
using UnityEngine;
using UnityEngine.UI;

namespace EzxrCore.Common
{
    public class DebugInfo : MonoBehaviour
    {
        public Text m_DebugText;
        private const float _updateInterval = 0.5F;

        private float _accum;
        private int _frames;
        private float _timeleft;
        private float _fps;

        private float logInterval = 1.0f; // Log interval in seconds
        private float timer = 0.0f;

        private void OnEnable()
        {
            _accum = 0;
            _frames = 0;
            _timeleft = 0;
            _fps = 0;

            //ECLog.SetOn();
        }

        private void onDisable()
        {
            //ECLog.SetOff();
        }

        void FixedUpdate()
        {

        }

        private void Update()
        {
            _timeleft -= Time.deltaTime;
            _accum += Time.timeScale / Time.deltaTime;

            ++_frames;

            if (_timeleft <= 0.0)
            {
                _fps = _accum / _frames;
                _timeleft = _updateInterval;
                _accum = 0.0F;
                _frames = 0;
            }

            timer += Time.deltaTime;

            if (timer >= logInterval)
            {
                //ECLog.AddDebugLog($"FPS: {_fps}", 0);
                //if (m_DebugText != null)
                //{
                //    m_DebugText.text = ECLog.GetDebugLog();
                //}
                timer = 0.0f;
            }
        }
    }
}