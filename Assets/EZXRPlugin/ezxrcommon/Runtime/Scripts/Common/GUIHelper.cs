

using UnityEngine;
namespace EzxrCore.Common
{
    public class GUIHelper
    {
        private static Color color = Color.red;
        private static float thickness = 4f;
        private static Texture2D texture;
        public static void DrawRect(Rect rect)
        {
            if (texture == null)
            {
                texture = new Texture2D(1, 1);
                texture.SetPixel(0, 0, color);
                texture.Apply();
            }
            // Draw the rectangle border
            GUI.DrawTexture(new Rect(rect.x, rect.y, rect.width, thickness), texture);
            GUI.DrawTexture(new Rect(rect.x, rect.yMax - thickness, rect.width, thickness), texture);
            GUI.DrawTexture(new Rect(rect.x, rect.y + thickness, thickness, rect.height - 2 * thickness), texture);
            GUI.DrawTexture(new Rect(rect.xMax - thickness, rect.y + thickness, thickness, rect.height - 2 * thickness), texture);
        }
    }
}