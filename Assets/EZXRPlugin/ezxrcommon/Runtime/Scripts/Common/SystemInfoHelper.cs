using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EzxrCore.Common
{
    public static class SystemInfoHelper
    {
        public static string GetDeivceName()
        {
           return SystemInfo.deviceName;
        }

        public static string GetDeviceModel()
        {
            return SystemInfo.deviceModel;
        }

        public static string GetDeviceUniqueIdentifier()
        {
            return SystemInfo.deviceUniqueIdentifier;
        }

        
    }
}