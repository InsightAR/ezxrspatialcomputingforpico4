using System;

namespace EzxrCore.Common
{
    public static class UnixTimestamp
    {
        public static double Now()
        {
            return (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
        //unix timestamp to DateTime str
        public static string ToDateTimeStr(double unixTimestamp)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dt = dt.AddSeconds(unixTimestamp).ToLocalTime();
            return dt.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}