﻿using UnityEngine;

namespace EzxrCore.Network.Http
{
	/**
     * \~english
     * Http response class
     * 
     * \~chinese
     * Http应答类
     */
    public class HttpResponse {
		/**
	     * \~english
	     * Status code
	     * 
	     * \~chinese
	     * 状态码
	     */
		public long status;
		/**
	     * \~english
	     * Http response body
	     * 
	     * \~chinese
	     * Http回答返回体
	     */
		public string body;
		// public byte[] rawBody;
		/**
	     * \~english
	     * Http response error message
	     * 
	     * \~chinese
	     * Http回答的错误信息
	     */
		public string error;

		public HttpResponse(long status, string body) {
			this.status = status;
			this.body = body;
			// this.rawBody = rawBody;
			this.error = null;
		}

		public HttpResponse(string error) {
			this.error = error;
		}

		// public T To<T>() {
		// 	return JsonUtility.FromJson<T> (body);
		// }

		/**
         * \~english
         * @brief Get the status of Http response, refer to Http status code
         *
         * \~chinese
         * @brief 获取Http回答的状态，参考Http状态码
         *
         */
		public long Status() {
			return status;
		}

		/**
         * \~english
         * @brief Get Http response body
         *
         * \~chinese
         * @brief 获取Http回答返回体
         *
         */
		public string Body() {
			return body;
		}

		// public byte[] RawBody() {
		// 	return rawBody;
		// }

		/**
         * \~english
         * @brief Determine whether the Http response is wrong
         *
         * \~chinese
         * @brief 判断Http回答是否有错误
         *
         */
		public bool IsOK() {
			return status >= 200 && status < 300 && error == null;
		}

		/**
         * \~english
         * @brief Determine whether the Http response is wrong
         *
         * \~chinese
         * @brief 获取Http回答的错误信息
         *
         */
		public string Error() {
			return error;
		}

		public override string ToString() 
        {
			return "status: " + status.ToString () + " - response: " + body.ToString ();
		}
	}
}
