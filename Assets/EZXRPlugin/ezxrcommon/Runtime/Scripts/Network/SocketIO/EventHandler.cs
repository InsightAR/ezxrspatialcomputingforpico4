﻿using EzxrCore.Network.SocketIOClient.Arguments;

namespace EzxrCore.Network.SocketIOClient
{
    public delegate void EventHandler(ResponseArgs args);
}
