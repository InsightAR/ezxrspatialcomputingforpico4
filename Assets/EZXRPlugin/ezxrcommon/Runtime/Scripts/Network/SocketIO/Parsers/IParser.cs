﻿using System.Threading.Tasks;

namespace EzxrCore.Network.SocketIOClient.Parsers
{
    public interface IParser
    {
        Task ParseAsync(ResponseTextParser rtp);
    }
}
