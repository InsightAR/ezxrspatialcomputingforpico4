﻿namespace EzxrCore.Network.SocketIOClient
{
    public enum SocketIOProtocol
    {
        Connect,
        Disconnect,
        Event,
        Ack,
        Error,
        BinaryEvent,
        BinaryAck
    }
}
