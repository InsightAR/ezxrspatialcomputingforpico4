﻿namespace EzxrCore.Network.SocketIOClient
{
    public enum SocketIOState
    {
        None,
        Connected,
        Closed
    }
}
