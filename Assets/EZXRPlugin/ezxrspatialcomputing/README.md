# EZXR Spatial Computing

EZXR Spatial Computing 易现空间计算Unity Package manager。

## 对应版本

| Unity Version | EZXR Spatial Computing |
| ------------- | ----------- |
| 2022.3        | 0.1.1       |

## Release Note

### V0.1.1

1. 提供iOS/android/MacOS（ARM64） EZXR Spatial Computing 易现空间计算 SDK
2. 提供C#调用算法接口SpatialComputingController类

## API 说明
