

using System;
using UnityEngine;
namespace EZXRCoreExtensions.SpatialComputing
{
    public class CpuImageControllerBase : MonoBehaviour
    {
        public delegate void ImageFrameDelegate(Matrix4x4 trackFrameTransform, SC_InputImage input_image, TrackState imageFrameState, double timestamp, ScreenOrientation orientation);
        public event ImageFrameDelegate OnImageFrame;

        public delegate void TrackFrameDelegate(Matrix4x4 trackFrameTransform, TrackState trackFrameState, double timestamp, ScreenOrientation orientation);
        public event TrackFrameDelegate OnTrackFrame;

        public delegate void TrackToImagePoseDelegate(Matrix4x4 track_to_image_pose);
        public event TrackToImagePoseDelegate OnTrackToImagePose;

        [NonSerialized]
        public Action<Texture> m_OnCameraTextureUpdated = null;
        
        public virtual TrackState GetTrackingState()
        {
            return TrackState.detecting;
        }

        public virtual void StopVPS() { }

        protected void OnImageFrameProcess(Matrix4x4 trackFrameTransform, SC_InputImage input_image, TrackState imageFrameState, double timestamp, ScreenOrientation orientation)
        {
            OnImageFrame?.Invoke(trackFrameTransform, input_image, imageFrameState, timestamp, orientation);
        }

        protected void OnTrackFrameProcess(Matrix4x4 trackFrameTransform, TrackState trackFrameState, double timestamp, ScreenOrientation orientation)
        {
            OnTrackFrame?.Invoke(trackFrameTransform, trackFrameState, timestamp, orientation);
        }

        protected void OnTrackToImagePoseOffset(Matrix4x4 track_to_image_pose)
        {
            OnTrackToImagePose?.Invoke(track_to_image_pose);
        }
    }
}

