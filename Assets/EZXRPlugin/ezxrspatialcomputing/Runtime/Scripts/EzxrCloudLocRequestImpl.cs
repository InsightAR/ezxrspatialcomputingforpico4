using System;
using System.Runtime.InteropServices;

namespace EZXRCoreExtensions.SpatialComputing
{
    public struct EzxrCloudLocRequestImpl
    {
        public SC_CloudLocRequestMeta meta;  // query struct
        public string jpgStr;         // jpg string
        public string requestInfoStr;

        public EzxrCloudLocRequestImpl(SC_CloudLocRequest requestData)
        {
            if (requestData.byte_length == 0 || requestData.jpg_ptr == IntPtr.Zero)
            {
                this.jpgStr = "";
            }
            else
            {
                // byte[] imageBytes = new byte[requestData.byte_length];
                // Marshal.Copy(requestData.jpg_ptr, imageBytes, 0, requestData.byte_length);
                // byte[] jpgBytes = ImageConversion.EncodeArrayToJPG(imageBytes, GraphicsFormat.B8G8R8_UNorm, width, height);
                byte[] jpgBytes = EzxrCloudLocalization.Instance.GetJPEGArray(requestData);
                this.jpgStr = Convert.ToBase64String(jpgBytes);
                //  Marshal.FreeHGlobal(requestData.jpgPtr); native自己会释放
            }

            if (requestData.request_info_length == 0 || requestData.request_info_ptr == IntPtr.Zero)
            {
                this.requestInfoStr = "";
            }
            else
            {
                byte[] protoBytes = new byte[requestData.request_info_length];
                Marshal.Copy(requestData.request_info_ptr, protoBytes, 0, requestData.request_info_length);
                this.requestInfoStr = Convert.ToBase64String(protoBytes);
                //   Marshal.FreeHGlobal(requestData.reqestInfoPtr);// native自己会释放
            }

            this.meta = requestData.meta;
        }
    }
}
