
using System;
using System.Runtime.InteropServices;
using EzxrCore.Common;
using EzxrCore.Network.WebRequest;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace EZXRCoreExtensions.SpatialComputing
{
    [Serializable]
    public class RequestData
    {
        [Serializable]
        public class Alg
        {
            public string imageEncodingData;
            public string protobufEncodingData;
        }

        public Alg alg;
        public long timestamp;
        public string sign;

        public RequestData()
        {
            alg = new Alg();
        }
    }
    [Serializable]
    public class ResponeData
    {
        [Serializable]
        public class Result
        {
            public string protobufEncodingData;
            public int algCode;
        }
        public string code;
        public string msg;
        public Result result;
    }

    public class EzxrCloudLocalization : EzxrCore.Common.Singleton<EzxrCloudLocalization>
    {
        public delegate void CloudLocalizationResponeDelegate(SC_CloudLocResult cloudLocResult);
        public event CloudLocalizationResponeDelegate OnResponeReceived;

        public string m_url;

        private int m_width;
        private int m_height;

        public override void Initialize()
        {
            base.Initialize();
        }
        
        public void SetWidthAndHeight(int width, int height)
        {
            m_width = width;
            m_height = height;
        }

        public byte[] GetJPEGArray(SC_CloudLocRequest requestData)
        {
            //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            //stopwatch.Start();
            byte[] byteArray = new byte[requestData.byte_length];
            Marshal.Copy(requestData.jpg_ptr, byteArray, 0, requestData.byte_length);
            byte[] nativeByteMirrorYArray = Texture2DHelper.FilpY(byteArray, m_width, m_height,3);
            //Debug.Log("requestData.byte_length: " + requestData.byte_length + " width " + m_width + " height " + m_height);
            byte[] jpgBytes = ImageConversion.EncodeArrayToJPG( nativeByteMirrorYArray, GraphicsFormat.B8G8R8_UNorm, (uint)m_width, (uint)m_height, (uint)m_width * 3);
            //stopwatch.Stop();
            //TimeSpan elapsedTime = stopwatch.Elapsed;
            //Debug.Log("GetJPEGArray - elapsedTime: " + elapsedTime.TotalMilliseconds);
            //EzxrCore.Log.ECLog.SaveFile(byteArray, "test.bin");
            //EzxrCore.Log.ECLog.SaveFile(jpgBytes, "test.jpg");
            return jpgBytes;            
        }

        public void RequestCloudLocDirectly(SpatialComputing.EzxrCloudLocRequestImpl requestData)
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long time = (long)(DateTime.UtcNow - epochStart).TotalMilliseconds;
            long requestT = time;
            string nonce = Guid.NewGuid().ToString().Substring(0, 8);

            var tk = "";

            string signature = "cloud.reloc|" + nonce + "|" + requestT + "|" + tk;
            string hashSha256 = EncodeUtility.Sha256(signature).ToLower();

            RequestData data = new RequestData();
            data.alg.imageEncodingData = requestData.jpgStr;
            data.alg.protobufEncodingData = requestData.requestInfoStr;
            data.timestamp = requestT;
            data.sign = hashSha256;

            string data4Send = JsonUtility.ToJson(data);
            //Debug.Log("RequestCloudLocDirectly - imageEncodingData.Length: " + data.alg.imageEncodingData.Length + ", protobufEncodingData: " + data.alg.protobufEncodingData);
            UnityWebRequest.Instance.SetRequestHeader("Content-Type", "application/json");
            UnityWebRequest.Instance.Create(m_url, data4Send, RequestMode.POST, GetData, "RequestCloudLocDirectly");
        }

        private void GetData(string data, string identifier, long statusCode = 200)
        {
            Debug.Log("RequestCloudLocDirectly - GetData: " + data);
            ResponeData responeResult = JsonUtility.FromJson<ResponeData>(data);
            HandleSingleCloudLocResult(responeResult.result.protobufEncodingData, responeResult.result.algCode.ToString());
        }

        private void HandleSingleCloudLocResult(string protoData, string algCode)
        {
            if (protoData == null)
            {
                Debug.LogError("HandleSingleCloudLocResult - protoData is null ");
                return;
            }
            int algCodeNum = int.Parse(algCode);
            if (algCodeNum >= 9000)
            {
                Debug.LogError("algCode >= 9000, error code: " + algCode);
                return;
            }
    
            SC_CloudLocResult ezxrCloudLocResult = new SC_CloudLocResult();
            SC_CloudLocResultMeta ezxrCloudLocResultMeta = new SC_CloudLocResultMeta();
            ezxrCloudLocResult.meta = ezxrCloudLocResultMeta;
            try
            {
                //base64 解码
                byte[] buffer = Convert.FromBase64String(protoData);
                int length = buffer.Length;
                IntPtr resultPtr = Marshal.AllocHGlobal(length);
                Marshal.Copy(buffer, 0, resultPtr, length);
                ezxrCloudLocResult.result_info_ptr = resultPtr;
                ezxrCloudLocResult.result_length = length;

                OnResponeReceived?.Invoke(ezxrCloudLocResult);
                
                //供底层算法调用之后，释放指针
                Marshal.FreeHGlobal(resultPtr);
            }
            catch (FormatException exp)
            {
                Debug.LogError("HandleSingleCloudLocResult - FormatException: " + exp.Message);
            }
        }
    }
}

 
        