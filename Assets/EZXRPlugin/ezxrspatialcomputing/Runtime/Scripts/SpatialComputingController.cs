using System;
using UnityEngine;
using EzxrCore.Common;
using AOT;
using UnityEngine.UI;

namespace EZXRCoreExtensions.SpatialComputing
{
    public class SpatialComputingController : MonoBehaviour
    {
        /// <summary>
        /// 定位资源的URL
        /// </summary>
        public string m_url = "https://reloc-gw.easexr.com/api/alg/cloud/aw/reloc/proxy?routeApp=parkc&map=arglass";
        public bool m_vpsRequestAuto = true;
        public CpuImageControllerBase m_cpuImageController;
        public bool m_imgUndistOnDevice = false;
        public DeviceType m_deviceType = DeviceType.EzxrGlass6dof;
        public string m_sdk_version = "1.1.0";
        public SC_VPSResultState currentVpsResultState = new SC_VPSResultState();
        [SerializeField]
        Transform m_CameraOffsetTransform;

        private IntPtr m_spatialComputingHandle;
        private bool m_vpsRequestManualOnce = false;
        private double m_timestampNow = 0;
        private Matrix4x4 m_cameraTransformNow;

        private Matrix4x4 m_trackFrameTransformNow;
        private Matrix4x4 m_trackFrameToCameraOffset = Matrix4x4.identity;
  
        private CopyStreamingAssets m_CopyStreamingAssets;
        private bool m_isCopyCompleted = false;

        // ForDebug
        public Text debugText;

        public GameObject Art;
        public GameObject testMap;

        private static bool hasStopVPS = false;
        private int vpsSuccessCount = 0;

        void Start()
        {
            if (m_cpuImageController != null)
            {
                m_cpuImageController.OnImageFrame += OnImageFrameReceived;
                m_cpuImageController.OnTrackFrame += OnTrackFrameReceived;
                m_cpuImageController.OnTrackToImagePose += OnTrackToImagePoseReceived;
            }

            EzxrCloudLocalization.Instance.OnResponeReceived += OnCloudLocResponseReceived;
            EzxrCloudLocalization.Instance.m_url = m_url;
            if (m_CopyStreamingAssets == null)
            {
                m_CopyStreamingAssets = gameObject.GetComponent<CopyStreamingAssets>();
                if (m_CopyStreamingAssets != null)
                {
                    m_CopyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
                }
            }
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                m_deviceType = DeviceType.ARKit;
            }
            Loom.Initialize();
            
        }

        void OnDestroy()
        {
            if (m_cpuImageController != null)
            {
                m_cpuImageController.OnImageFrame -= OnImageFrameReceived;
                m_cpuImageController.OnTrackFrame -= OnTrackFrameReceived;
                m_cpuImageController.OnTrackToImagePose -= OnTrackToImagePoseReceived;
            }
            if (m_CopyStreamingAssets != null)
            {
                m_CopyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
            }
            if (m_spatialComputingHandle != IntPtr.Zero)
                SpatialComputing.ExternApi.scSpatialComputingDestroy(m_spatialComputingHandle);
            EzxrCloudLocalization.Instance.OnResponeReceived -= OnCloudLocResponseReceived;
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnCopyCompleted(string assetPath)
        {
            if (assetPath == null)
            {
                UnityEngine.Debug.LogError("configuration resources assetPath is error");
            }
            UnityEngine.Debug.Log("OnCopyCompleted : " + assetPath);
            // OnCopyCompleted : /storage/emulated/0/Android/data/com.ezxr.spatialcomputing.pxrdemo/files/EzxrCore/SpatialComputing/Case1

            m_spatialComputingHandle = SpatialComputing.ExternApi.scSpatialComputingCreate();
            if (m_spatialComputingHandle == IntPtr.Zero)
            {
                UnityEngine.Debug.LogError("scSpatialComputingCreate failed");
                return;
            }
            SpatialComputing.ExternApi.scSetRequestFrameCallingSC(m_spatialComputingHandle, OnCloudLocRequestCallback);
            SpatialComputing.ExternApi.scSetBoolDoUndistortSC(m_spatialComputingHandle, m_imgUndistOnDevice);   // false

            string configPath = assetPath + "/localizer_fusion_config.json";
            SpatialComputing.ExternApi.scSetSmoothConfig(m_spatialComputingHandle, configPath);
            string vps_configPath = assetPath + "/mock_vpsc_config.json";
            SpatialComputing.ExternApi.scSetVPSCConfig(m_spatialComputingHandle, vps_configPath);
            //set VPSDeviceInfoC
            VPSDeviceInfoC vpsDeviceInfoC = new VPSDeviceInfoC();
            vpsDeviceInfoC.device_brand = SystemInfoHelper.GetDeivceName();
            vpsDeviceInfoC.product_name = SystemInfoHelper.GetDeviceModel();
            vpsDeviceInfoC.SN_code = SystemInfoHelper.GetDeviceUniqueIdentifier();
            vpsDeviceInfoC.VIO_type = m_deviceType.ToString();
            vpsDeviceInfoC.SDK_version = m_sdk_version;
            SpatialComputing.ExternApi.scSetVPSDeviceInfoSC(m_spatialComputingHandle, vpsDeviceInfoC);
            // printf VPSDeviceInfoC
            Debug.Log("device_brand: " + vpsDeviceInfoC.device_brand + "\n" +
                        "product_name: " + vpsDeviceInfoC.product_name + "\n" +
                        "SN_code: " + vpsDeviceInfoC.SN_code + "\n" +
                        "VIO_type: " + vpsDeviceInfoC.VIO_type + "\n" +
                        "SDK_version: " + vpsDeviceInfoC.SDK_version + "\n");
        }

        public void SetVPSRequestManual(bool vpsRequestManualOnce)
        {
            m_vpsRequestManualOnce = vpsRequestManualOnce;
        }

        //for local Image test
        public void SendVPSRequestFrame(SC_InputFrame sc_input_frame)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scInputVPSRequestFrameSC(m_spatialComputingHandle, sc_input_frame);
            }
        }

        public void SendVPSTrackToImagePoseOffset(float[] trackToImagePoseOffset)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                var imageToHead = new float[]
                {
                    trackToImagePoseOffset[0], -trackToImagePoseOffset[1], -trackToImagePoseOffset[2], trackToImagePoseOffset[3],
                    trackToImagePoseOffset[4], -trackToImagePoseOffset[5], -trackToImagePoseOffset[6], trackToImagePoseOffset[7],
                    trackToImagePoseOffset[8], -trackToImagePoseOffset[9], -trackToImagePoseOffset[10], trackToImagePoseOffset[11],
                    0, 0, 0, 1
                };
                //Debug.Log($"PXR_Enterprise Analysis, imageToHead: [{imageToHead[0]},{imageToHead[1]},{imageToHead[2]},{imageToHead[3]}], " +
                //            $"[{imageToHead[4]},{imageToHead[5]},{imageToHead[6]},{imageToHead[7]}]," +
                //            $"[{imageToHead[8]},{imageToHead[9]},{imageToHead[10]},{imageToHead[11]}]," +
                //            $"[{imageToHead[12]},{imageToHead[13]},{imageToHead[14]},{imageToHead[15]}]");

                //Debug.Log($"PXR_Enterprise Analysis, trackToImagePoseOffset: [{trackToImagePoseOffset[0]},{trackToImagePoseOffset[1]},{trackToImagePoseOffset[2]},{trackToImagePoseOffset[3]}], " +
                //    $"[{trackToImagePoseOffset[4]},{trackToImagePoseOffset[5]},{trackToImagePoseOffset[6]},{trackToImagePoseOffset[7]}]," +
                //    $"[{trackToImagePoseOffset[8]},{trackToImagePoseOffset[9]},{trackToImagePoseOffset[10]},{trackToImagePoseOffset[11]}]," +
                //    $"[{trackToImagePoseOffset[12]},{trackToImagePoseOffset[13]},{trackToImagePoseOffset[14]},{trackToImagePoseOffset[15]}]");
                SpatialComputing.ExternApi.scSetExtrinsicImageToHead(m_spatialComputingHandle, imageToHead);
            }
        }

        public void GetTimestampAndCameraMatrix(out double timestamp, out Matrix4x4 cameraToWorldMatrix)
        {
            timestamp = m_timestampNow;
            cameraToWorldMatrix = m_cameraTransformNow;
        }

        public void TrigerApplyLocResultImmediately()
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scTrigerApplyLocResultImmediately(m_spatialComputingHandle);
            }
        }

        void OnTrackToImagePoseReceived(Matrix4x4 trackToImagePose)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                float[] trackToImagePoseOffset = SpatialComputing.Helper.GetTrackToImagePoseOffset(trackToImagePose);
                SendVPSTrackToImagePoseOffset(trackToImagePoseOffset);
            }
            m_trackFrameToCameraOffset = trackToImagePose;
        }

        void OnTrackFrameReceived(Matrix4x4 trackFrameTransform, TrackState trackState, double timestamp, ScreenOrientation orientation)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(trackFrameTransform, timestamp, orientation);
                //Debug.Log(SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC));

                FusePose3dMsgC fusePose3dMsgC = new FusePose3dMsgC();
                bool success = SpatialComputing.ExternApi.scInputTrackGetFuse(m_spatialComputingHandle, pose3dMsgC, trackState, out fusePose3dMsgC);

                //Debug.Log($"PXR_Enterprise, scInputTrackGetFuse result: {success}");

                // TODO:
                // 1. in & out 状态过滤；
                // 2. offsettransform 可能需要修改local transform
                if (success)
                {
                    Matrix4x4 offsetMatrix = SpatialComputing.Helper.GetOffsetMatrix(fusePose3dMsgC, orientation);
                    m_CameraOffsetTransform.position = offsetMatrix.GetColumn(3);
                    m_CameraOffsetTransform.rotation = offsetMatrix.rotation;
                }
            }

            m_cameraTransformNow = trackFrameTransform * m_trackFrameToCameraOffset;
            m_trackFrameTransformNow = trackFrameTransform;
        }
        void OnImageFrameReceived(Matrix4x4 trackFrameTransform, SC_InputImage input_image, TrackState trackState, double timestamp, ScreenOrientation orientation)
        {
            if (m_spatialComputingHandle == IntPtr.Zero) return;
            
            m_timestampNow = timestamp;

            EzxrCloudLocalization.Instance.SetWidthAndHeight(input_image.width, input_image.height);

            Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(trackFrameTransform, timestamp, orientation);
            //Debug.Log($"PXR_Enterprise, pose3dMsgC: {SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC)}");

            bool vpsRequestAuto = false;

            if(m_vpsRequestAuto == true)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, input_image, timestamp);
                SendVPSRequestFrame(sc_input_frame);
                vpsRequestAuto = true;
            }
      
            if (m_vpsRequestManualOnce == true && vpsRequestAuto == false)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, input_image, timestamp);
                //Debug.Log(SpatialComputing.Helper.SC_InputImageToString(sc_input_frame.input_image));
                SendVPSRequestFrame(sc_input_frame);
                m_vpsRequestManualOnce = false;
                //Debug.Log("m_vpsRequestManualOnce == true");
            }

            //Debug.Log($"PXR_Enterprise, OnImageFrameReceived");
            //Debug.Log($"PXR_Enterprise, m_url: {m_url}");
        }

        void OnCloudLocResponseReceived(SC_CloudLocResult response)
        {
            if (m_spatialComputingHandle == IntPtr.Zero) return;
            //Debug.Log("OnCloudLocResponseReceived byteLength=" + response.result_length.ToString());
            SC_VPSResultState vpsResultState =  SpatialComputing.ExternApi.scSetVPSResultCalledSC(m_spatialComputingHandle, response);
            currentVpsResultState = vpsResultState;
            //Debug.Log("OnCloudLocResponseReceived result=" + vpsResultState.vps_result_status);
            printfVPSResultState(vpsResultState);

            if (vpsResultState.vps_result_status == LOCSTATUS.SUCCESS)
            {
                Debug.Log($"PXR_Enterprise, SpatialComputingController StopVPS {hasStopVPS}");

                vpsSuccessCount = vpsSuccessCount + 1;
                if (vpsSuccessCount >= 2)
                {
                    Art.SetActive(true);
                    testMap.SetActive(false);
                    StopVPS();
                }

            }
        }

        void printfVPSResultState(SC_VPSResultState vpsResultState)
        {
            //EzxrCore.Log.ECLog.AddDebugLog($"ts: {vpsResultState.t_s}", 1);
            string status = "";
            switch(vpsResultState.vps_result_status) 
            {
               case LOCSTATUS.SUCCESS:
                    status = "定位成功，返回pose等信息";
                    break;
                case LOCSTATUS.FAIL_UNKNOWN:
                    status = "定位失败，走完了定位算法流程，但是图像无法定位到给定地图中";
                    break;
                case LOCSTATUS.FAIL_MATCH:
                    status = "定位失败，具体原因1 hy: not used";
                    break;
                case LOCSTATUS.FAIL_INLIER:
                    status = "定位失败，具体原因2 hy: not used";
                    break;
                case LOCSTATUS.INVALID_DEVICEINFO:
                    status = "数据不合法，传入的protobuf.deviceInfo不符合规范";
                    break;
                case LOCSTATUS.INVALID_LOCALIZER:
                    status = "数据不合法，部署阶段的localizer未成功初始化";
                    break;
                case LOCSTATUS.INVALID_IMAGE:
                    status = "数据不合法，传入的图像或protobuf.deviceInfo中出现不被接受的图像格式（仅接收通道数为1或3,且类型为CV_8U或CV_8UC3的图像）";
                    break;
                case LOCSTATUS.INVALID_IMAGE_PROTO_MATCH:
                    status = "数据不合法，传入的图像文件长度，与protobuf.deviceInfo中记录的图像字节数不匹配";
                    break;
                case LOCSTATUS.INVALID_MESSAGE:
                    status = "传入的message不合法";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_UNKNOWN:
                    status = "hy: not used";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_NOMAP:
                    status = "未加载完成可用的summary map hy: not used";
                    break;
            }
            //EzxrCore.Log.ECLog.AddDebugLog($"reason: {status}", 2);
            Debug.Log($"PXR_Enterprise, printfVPSResultState: {status}");

            //if (string.IsNullOrEmpty(status))
            //    debugText.text = vpsResultState.vps_result_status.ToString();
            //else
            //    debugText.text = status;
        }

        void StopVPS()
        {
            hasStopVPS = true;
            m_cpuImageController.StopVPS();
            Debug.Log($"PXR_Enterprise, SpatialComputingController hasStopVPS: {hasStopVPS}");
        }

        [MonoPInvokeCallback(typeof(SC_REQUEST_CLOUDLOC_CALLBACK))]
        public static void OnCloudLocRequestCallback(SpatialComputing.SC_CloudLocRequest request_data)
        {
            //Debug.Log("PXR_Enterprise, OnCloudLocRequestCallback" + request_data.meta.timestamp_s.ToString() + " " + request_data.byte_length.ToString() + " " + request_data.request_info_length.ToString());
            EzxrCloudLocRequestImpl request = new EzxrCloudLocRequestImpl(request_data);
            Loom.QueueOnMainThread(o =>
            {
                if (hasStopVPS) return;
                EzxrCloudLocalization.Instance.RequestCloudLocDirectly(request);
            }, null);
        }
    }
}