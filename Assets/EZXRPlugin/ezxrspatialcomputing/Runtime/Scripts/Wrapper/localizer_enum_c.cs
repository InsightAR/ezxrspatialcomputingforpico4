
namespace EZXRCoreExtensions.SpatialComputing
{
    public enum LOCSTATUS
    {
        SUCCESS = 0x01, //定位成功，返回pose等信息
        FAIL_UNKNOWN = 0x10, //定位失败，走完了定位算法流程，但是图像无法定位到给定地图中
        FAIL_MATCH = 0x11, //定位失败，具体原因1 hy: not used
        FAIL_INLIER = 0x12, //定位失败，具体原因2 hy: not used
        INVALID_DEVICEINFO = 0x20, //数据不合法，传入的protobuf.deviceInfo不符合规范
        INVALID_LOCALIZER = 0x21, //数据不合法，部署阶段的localizer未成功初始化
        INVALID_IMAGE = 0x22, //数据不合法，传入的图像或protobuf.deviceInfo中出现不被接受的图像格式（仅接收通道数为1或3,且类型为CV_8U或CV_8UC3的图像）
        INVALID_IMAGE_PROTO_MATCH = 0x23, //数据不合法，传入的图像文件长度，与protobuf.deviceInfo中记录的图像字节数不匹配
        INVALID_MESSAGE = 0x24, //传入的message不合法
        FAIL_SUMMARY_UNKNOWN = 0x30, //hy: not used
        FAIL_SUMMARY_NOMAP = 0x31 //未加载完成可用的summary map hy: not used
    }
}
