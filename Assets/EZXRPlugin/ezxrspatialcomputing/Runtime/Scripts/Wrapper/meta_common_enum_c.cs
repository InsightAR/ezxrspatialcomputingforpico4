using System.Runtime.InteropServices;

namespace EZXRCoreExtensions.SpatialComputing
{
    public enum PoseType
    {
        unkown = 0,
        track = 1,
        visual_cloud = 2,
        visual_device = 3,
        gps = 4,
        bluetooth = 5,
        wlan = 6,
        fuse = 7,
        fuse_smooth = 8
    }

    public enum TrackState
    {
        // pose actually not available
        detecting = 2,
        // pose good
        tracking = 3,
        // pose low quality
        track_limited = 4,
    }

    public enum DeviceType
    {
        ARKit = 0,
        ARCore = 1,
        HWAREngine = 2,
        Ezxr6dof = 3,
        EzxrGlass6dof = 4
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SC_VPSResultState
    {
        // 如果时间戳小于0，说明是无效时间戳 
        public double t_s; //unix时间戳，以s为单位，精确到小数点后3或4位
        public LOCSTATUS vps_result_status;
    }

}