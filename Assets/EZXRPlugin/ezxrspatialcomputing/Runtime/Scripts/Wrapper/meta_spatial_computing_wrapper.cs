using System;
using System.Runtime.InteropServices;

namespace EZXRCoreExtensions.SpatialComputing
{
    public static partial class ExternApi
    {
#if (UNITY_IOS || UNITY_TVOS || UNITY_WEBGL) && !UNITY_EDITOR
        public const string LIBRARY_NAME = "__Internal";
#else
        public const string LIBRARY_NAME = "spatial_computing";
#endif

        [DllImport(LIBRARY_NAME)]
        public static extern int scSpatialComputingGetVesion();

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr scSpatialComputingCreate();

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSpatialComputingDestroy(IntPtr pt_wrapper);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool scSetFusionConfig(IntPtr pt_wrapper, string fusion_config_path, DeviceType device_type);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool scSetGeoPath(IntPtr pt_wrapper, string geo_config_path);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSetExtrinsicImageToHead(IntPtr pt_wrapper, float[] T_image_to_head);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool scInputTrackGetFuse(IntPtr pt_wrapper, Pose3dMsgC pose, TrackState track_state, out FusePose3dMsgC out_pose);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scInputGPSPose(IntPtr pt_wrapper, Pose2dMsgC pose, double xy_t_s, double compass_t_s);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scInputBPSPose(IntPtr pt_wrapper, Pose2dMsgC pose, double xy_t_s, double compass_t_s);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool scSetVPSCConfig(IntPtr pt_wrapper, string vpsc_config_path);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSetRequestFrameCallingSC(IntPtr pt_wrapper, SC_REQUEST_CLOUDLOC_CALLBACK func);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern SC_VPSResultState scSetVPSResultCalledSC(IntPtr pt_wrapper, SC_CloudLocResult response_frame);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scInputVPSRequestFrameSC(IntPtr pt_wrapper, SC_InputFrame vps_frame);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scQueryVpsResultStateSC(IntPtr pt_wrapper, out SC_VPSResultState output_state);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSetSmoothConfig(IntPtr pt_wrapper, string smooth_config_path);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scTrigerApplyLocResultImmediately(IntPtr pt_wrapper);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSetBoolDoUndistortSC(IntPtr pt_wrapper, bool do_or_not);
        
        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void scSetVPSDeviceInfoSC(IntPtr pt_wrapper, VPSDeviceInfoC info);
        
    }
}
