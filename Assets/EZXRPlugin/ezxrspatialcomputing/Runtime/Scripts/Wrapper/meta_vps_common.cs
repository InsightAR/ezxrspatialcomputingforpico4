using System;
using System.Runtime.InteropServices;
namespace EZXRCoreExtensions.SpatialComputing
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SC_CloudLocRequestMeta
    {
        public double timestamp_s; //unix时间戳，以s为单位，精确到小数点后3位
    }
    
    [StructLayout(LayoutKind.Sequential)]
    public struct SC_CloudLocRequest
    {
        public SC_CloudLocRequestMeta meta;
        public IntPtr jpg_ptr;
        public int byte_length;
        
        public IntPtr request_info_ptr;
        public int request_info_length;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SC_CloudLocResultMeta
    {
        // 留空，以后可以扩展；
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SC_CloudLocResult
    {
        public SC_CloudLocResultMeta meta;
        public IntPtr result_info_ptr;
        public int result_length;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SC_InputFrame
    {
        public SC_InputImage input_image;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public float[] T_cv_head_to_world;
        public bool valid_T_cv_head_to_world;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct VPSDeviceInfoC {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string device_brand;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string product_name;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string SN_code;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string VIO_type;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string SDK_version;
    };


    public delegate void SC_REQUEST_CLOUDLOC_CALLBACK(SC_CloudLocRequest request_data);
    
}