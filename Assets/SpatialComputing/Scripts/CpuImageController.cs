using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXRCoreExtensions.SpatialComputing;
using Unity.XR.PXR;
using EzxrCore.Common;

namespace EZXR.Glass.SpatialComputing 
{

    public struct ImageFrameData 
    {
        // YUV420, 图像format先留空
        public IntPtr fullImage;       
        
        // image height
        public int    width;

        // image width
        public int    height;

        // pinhole: k1, k2, p1, p2;
        // fisheye: k1, k2, k3, k4;
        public float[] distortionCoefficient; 

        // 0: pinhole,  1: fisheye;
        public int distortionType;      

        // intrinsics,  fx,fy,cx,cy;
        public float[] intrinsics;        

        // image time stamp
        public double imageTime;

        // head pose accoridng to the image timestamp;
        public Pose headPoseImgTs;       

        // head pose on image-camera
        public float[] imageToHeadPoseOffset;
    
        public static ImageFrameData Create() {
            return new ImageFrameData(){
                fullImage = IntPtr.Zero,
                width = 0,
                height = 0,
                distortionCoefficient = new float[4] {0,0,0,0},
                distortionType = 0,
                intrinsics = new float[4] {0,0,0,0},
                imageTime = 0,
                headPoseImgTs = Pose.identity,
                imageToHeadPoseOffset = new float[16]
            };
        }

        public override string ToString()
        {
            return string.Format($"width:{width}, height:{height}, " + 
                    $"distortionCoefficient:{distortionCoefficient[0]}, {distortionCoefficient[1]}, {distortionCoefficient[2]}, {distortionCoefficient[3]}, " +
                    $"distortionType:{distortionType}, " +
                    $"imageTime:{imageTime}, " +
                    $"headPoseImgTs pos:{headPoseImgTs.position.x}, {headPoseImgTs.position.y}, {headPoseImgTs.position.z}, " + 
                    $"headPoseImgTs rot:{headPoseImgTs.rotation.x}, {headPoseImgTs.rotation.y}, {headPoseImgTs.rotation.z}, {headPoseImgTs.rotation.w}, " + 
                    $"imageToHeadPoseOffset: row0 {imageToHeadPoseOffset[0]}, {imageToHeadPoseOffset[1]}, {imageToHeadPoseOffset[2]}, {imageToHeadPoseOffset[3]}, " + 
                    $"row1 {imageToHeadPoseOffset[4]}, {imageToHeadPoseOffset[5]}, {imageToHeadPoseOffset[6]}, {imageToHeadPoseOffset[7]}, " +
                    $"row2 {imageToHeadPoseOffset[8]}, {imageToHeadPoseOffset[9]}, {imageToHeadPoseOffset[10]}, {imageToHeadPoseOffset[11]}, " +
                    $"row3 {imageToHeadPoseOffset[12]}, {imageToHeadPoseOffset[13]}, {imageToHeadPoseOffset[14]}, {imageToHeadPoseOffset[15]}"
                    );
        }
    }

    public class CpuImageController : CpuImageControllerBase
    {
        private bool m_fisrtFrameArrived = true;
        private double m_timestampDelta = 0;
        private double m_timestampNow = 0;

        private Matrix4x4 headToImage = Matrix4x4.identity;

        public EZXRSpatialComputingManager scManager;

        private int framecount = 0;

        private bool hasStopVPS = false;

        public override void StopVPS()
        {
            hasStopVPS = true;

            if (scManager) scManager.StopVPS();

            Debug.Log($"PXR_Enterprise, CpuImageController hasStopVPS: {hasStopVPS}");

        }

        void LateUpdate() {


            ScreenOrientation orientation = ScreenOrientation.LandscapeLeft;

            // 获取上屏时刻的预测时间戳
            double predictTime = PXR_Enterprise.GetPredictedDisplayTime();
            // 获取上屏时刻的sensor 状态和位姿
            SensorState sensorState = PXR_Enterprise.GetPredictedMainSensorState(predictTime);
            // 显示上屏时刻预测位姿
            var unityPose = PXR_EnterprisePlugin.ToUnityPose(sensorState.pose);
            Matrix4x4 trackFrameTransform = Matrix4x4.TRS(
                unityPose.position,
                unityPose.rotation,
                Vector3.one
            );

            double timestamp = (predictTime * 1e-3);    //毫秒
            //Debug.Log($"PXR_Enterprise, TrackPose timestamp_ns = {predictTime}, changeto timestamp_s = {timestamp}");

            TrackState state = sensorState.status == 1 ? TrackState.tracking : TrackState.detecting;
            if (sensorState.status != 1) Debug.Log($"PXR_Enterprise, sensorState.status = {sensorState.status}");
            OnTrackFrameProcess(trackFrameTransform, state, timestamp, orientation);
        }

        void OnImageFrameDataRecieved(ImageFrameData frameData) {

            // Debug.Log(" xnh CpuImageController:OnImageFrameDataRecieved " + frameData);

            if(m_fisrtFrameArrived) {
                Matrix4x4 imageToHead = new Matrix4x4();
                imageToHead.SetRow(0, new Vector4(frameData.imageToHeadPoseOffset[0],  frameData.imageToHeadPoseOffset[1], frameData.imageToHeadPoseOffset[2], frameData.imageToHeadPoseOffset[3]));
                imageToHead.SetRow(1, new Vector4(frameData.imageToHeadPoseOffset[4],  frameData.imageToHeadPoseOffset[5], frameData.imageToHeadPoseOffset[6], frameData.imageToHeadPoseOffset[7]));
                imageToHead.SetRow(2, new Vector4(frameData.imageToHeadPoseOffset[8],  frameData.imageToHeadPoseOffset[9], frameData.imageToHeadPoseOffset[10], frameData.imageToHeadPoseOffset[11]));
                imageToHead.SetRow(3, new Vector4(frameData.imageToHeadPoseOffset[12],  frameData.imageToHeadPoseOffset[13], frameData.imageToHeadPoseOffset[14], frameData.imageToHeadPoseOffset[15]));

                headToImage = imageToHead;
                OnTrackToImagePoseOffset(headToImage);
                //m_fisrtFrameArrived = false;

            }

            // Matrix4x4 imageCamTransform;
            // {
                Matrix4x4 headTransform = Matrix4x4.TRS(
                    new Vector3(frameData.headPoseImgTs.position.x, frameData.headPoseImgTs.position.y, frameData.headPoseImgTs.position.z),
                    new Quaternion(frameData.headPoseImgTs.rotation.x, frameData.headPoseImgTs.rotation.y, frameData.headPoseImgTs.rotation.z, frameData.headPoseImgTs.rotation.w),
                    new Vector3(1, 1, 1));
            //     imageCamTransform = headTransform * headToImage;   
            // }

            // timestamp
            double timestamp = frameData.imageTime;

            // assemble SC_InputImage
            SC_InputImage inputImage = new SC_InputImage();
            {
                inputImage.format = SC_ImageFormat.SC_Image_RGB_888;
                inputImage.ptr0 = frameData.fullImage;
                inputImage.len_ptr0 = (int)(3 * frameData.width * frameData.height);
                //Debug.Log($"PXR_Enterprise, len_ptr0: {inputImage.len_ptr0}");
                inputImage.ptr1 = IntPtr.Zero;
                inputImage.len_ptr1 = 0;

                inputImage.width = (int)(frameData.width);
                inputImage.height = (int)(frameData.height);
                
                inputImage.fx = frameData.intrinsics[0];
                inputImage.fy = frameData.intrinsics[1];
                inputImage.cx = frameData.intrinsics[2];
                inputImage.cy = frameData.intrinsics[3];
                inputImage.timestamp_s = timestamp;

                inputImage.k1 = frameData.distortionCoefficient[0];     //0 0 0 0
                inputImage.k2 = frameData.distortionCoefficient[1];
                inputImage.k3_p1 = frameData.distortionCoefficient[2];
                inputImage.k4_p2 = frameData.distortionCoefficient[3];
                inputImage.camera_type = SC_CameraType.SC_PINHOLE;

                //Debug.Log($"PXR_Enterprise, VST inputImage: w{inputImage.width}h{inputImage.height}, fx{inputImage.fx}fy{inputImage.fy}cx{inputImage.cx}cy{inputImage.cy},ts{inputImage.timestamp_s}," +
                //    $"k1{inputImage.k1}k2{inputImage.k2}p1{inputImage.k3_p1}p2{inputImage.k4_p2},type:{inputImage.camera_type}");
            }
            // trackingstate
            TrackState trackState = TrackState.tracking;

            //
            OnImageFrameProcess(headTransform, inputImage, trackState, timestamp, ScreenOrientation.LandscapeLeft);
        }

        void OnEnable() {
            if(scManager)
                scManager.OnImageFrameData += OnImageFrameDataRecieved;
        }

        void OnDisable() {
            if(scManager)
                scManager.OnImageFrameData -= OnImageFrameDataRecieved;
        }
    }
}

