using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using EZXR.Glass;
using Unity.XR.PXR;
using UnityEngine.UI;
using EzxrCore.Common;

namespace EZXR.Glass.SpatialComputing
{
    public enum LocCam
    {
            LocCam_RGB,
            // LocCam_FisheyeGray
    }

    public class EZXRSpatialComputingManager : MonoBehaviour
    {
        private int width = 2328;
        private int height = 1748;

        public delegate void ImageFrameDataDelegate(ImageFrameData imageFrameData);

        public event ImageFrameDataDelegate OnImageFrameData;

        public LocCam locCamType = LocCam.LocCam_RGB;


        private bool hasExtractOST = false;
        //private OSTMatrices ostMatrices = new OSTMatrices();
        private float[] ostMatrices = new float[16];
        private float[] imageIntrinsics = new float[4];
        Pose imageTsHeadPose;

        private ImageFrameData imageFrameData = ImageFrameData.Create();

        // ForDebug
        public Text debugText;
        public RawImage rawImageAntiDistortion;
        public GameObject HMDCamRed;  // 红色，显示 Camera.main.transform globalPose 数据来源head globalPose
        public GameObject APICamBlue; // 蓝色，显示上屏时刻的预测 head globalPose
        public GameObject RGBCamGreen;// 绿色，显示 AcquireVSTCameraFrameAntiDistortion接口获取图像时刻的 head globalPose

        private Texture2D texture2D;

        private void OnEnable()
        {
            if (locCamType == LocCam.LocCam_RGB)
            {
                StartCoroutine(startRGBCamera());
            }
            startTrackSession();
        }

        private void OnDisable()
        {
            stopTrackSession();
            if (locCamType == LocCam.LocCam_RGB)
            {
                stopRGBCamera();
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            PXR_Boundary.EnableSeeThroughManual(true);          // 打开全局seethrgouth
            PXR_Plugin.Boundary.UPxr_SetSeeThroughState(true); // 设置全局坐标系(Unity Camera.main.transform 数据来源：head globalPose，此时与AcquireVSTCameraFrameAntiDistortion返回值 frame.rgbCameraPose)

            var result = PXR_Enterprise.InitEnterpriseService();         // 初始化

            texture2D = new Texture2D(width, height, TextureFormat.RGB24, false, false);
            Debug.Log($"PXR_Enterprise, InitEnterpriseService: {result}");
        }

        void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                PXR_Boundary.EnableSeeThroughManual(true); // 打开全局 seethrgouth
            }
        }

        private void startTrackSession()
        {
            //Invoke("RelocByTrack2d", 2.0f);
            InvokeRepeating("UpdateCameraImage", 0.5f, 1.0f);
        }

        private void stopTrackSession()
        {

        }
        private IEnumerator startRGBCamera()
        {
            yield return null;
            yield return null;
            yield return null;
            bool result = PXR_Enterprise.OpenVSTCamera();
            Debug.Log($"PXR_Enterprise, OpenVSTCamera: {result}");

        }

        private void stopRGBCamera()
        {
            bool result = PXR_Enterprise.CloseVSTCamera();
            Debug.Log($"PXR_Enterprise, CloseVSTCamera: {result}");
        }

        // test save images
        /*
        Texture2D texture0 = null;
        Texture2D texture1 = null;
        Texture2D texture2 = null;
        byte[] dataU;
        byte[] dataV;
        int count = 0;
        byte[] byteArray = null;
        */

        private bool hasStopVPS = false;

        public void StopVPS()
        {
            hasStopVPS = true;

            Debug.Log($"PXR_Enterprise, EZXRSpatialComputingManager hasStopVPS: {hasStopVPS}");

        }

        private void UpdateCameraImage()
        {

            if (hasStopVPS) return;

            if (!hasExtractOST)
            {
                var cameraParams = PXR_Enterprise.GetCameraParameters();
                //Debug.Log($"PXR_Enterprise, cameraParams: fx{cameraParams.fx},fy{cameraParams.fy},cx{cameraParams.cx},cy{cameraParams.cy},x{cameraParams.x},y{cameraParams.y}," +
                //    $"z{cameraParams.z},rx{cameraParams.rx},ry{cameraParams.ry},rz{cameraParams.rz},rw{cameraParams.rw}");

                // set render camera fov
                Camera.main.fieldOfView = 2 * Mathf.Atan((float)(cameraParams.cy / cameraParams.fy)) * Mathf.Rad2Deg;
                //Debug.Log($"PXR_Enterprise, set Main Camera FOV: {Camera.main.fieldOfView}");

                imageIntrinsics[0] = (float)cameraParams.fx;
                imageIntrinsics[1] = (float)cameraParams.fy;
                imageIntrinsics[2] = (float)cameraParams.cx;
                imageIntrinsics[3] = (float)cameraParams.cy;

                var cvMatrix4x4 = new Matrix4x4();
                cvMatrix4x4.SetTRS(new Vector3((float)cameraParams.x, (float)cameraParams.y, (float)cameraParams.z),
                                    new Quaternion((float)cameraParams.rx, (float)cameraParams.ry, (float)cameraParams.rz, (float)cameraParams.rw),
                                    Vector3.one);

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        ostMatrices[4 * i + j] = cvMatrix4x4[i, j];
                    }
                }

            }

            if (locCamType == LocCam.LocCam_RGB)
            {

                {
                    bool res = false;

                    Frame frame = PXR_Enterprise.AcquireVSTCameraFrameAntiDistortion(width, height);

                    //texture2D.LoadRawTextureData(frame.data, (int)frame.datasize);
                    //texture2D.Apply();

                    //if (res)
                    {

                        // assemble image frame data
                        AssembleImageFrameData(frame);

                        if (frame.width > 0 && frame.height > 0 && frame.data != IntPtr.Zero)
                            // distribute image frame data
                            if (OnImageFrameData != null) OnImageFrameData(imageFrameData);


                    }
                }
            }

            // if (locCamType == LocCam.LocCam_FisheyeGray)
            // {
            //     // to be impl
            // }
        }

        private void AssembleImageFrameData(Frame frame) {

            imageFrameData.fullImage = frame.data;
            imageFrameData.width = (int)frame.width;
            imageFrameData.height = (int)frame.height;
            imageFrameData.distortionCoefficient[0] = 0;                        
            imageFrameData.distortionCoefficient[1] = 0;
            imageFrameData.distortionCoefficient[2] = 0;
            imageFrameData.distortionCoefficient[3] = 0;
            imageFrameData.distortionType = 0; // pinhole;
            imageFrameData.intrinsics[0] = imageIntrinsics[0];
            imageFrameData.intrinsics[1] = imageIntrinsics[1];
            imageFrameData.intrinsics[2] = imageIntrinsics[2];
            imageFrameData.intrinsics[3] = imageIntrinsics[3];
            imageFrameData.imageTime = (double)frame.timestamp * 1e-9;

            var unityPose = PXR_EnterprisePlugin.ToUnityPose(frame.pose);
            imageFrameData.headPoseImgTs.position = unityPose.position;
            imageFrameData.headPoseImgTs.rotation = unityPose.rotation;

            Array.Copy(ostMatrices, imageFrameData.imageToHeadPoseOffset, ostMatrices.Length);

        }
    }
}
