﻿/*******************************************************************************
Copyright © 2015-2022 PICO Technology Co., Ltd.All rights reserved.  

NOTICE：All information contained herein is, and remains the property of 
PICO Technology Co., Ltd. The intellectual and technical concepts 
contained herein are proprietary to PICO Technology Co., Ltd. and may be 
covered by patents, patents in process, and are protected by trade secret or 
copyright law. Dissemination of this information or reproduction of this 
material is strictly forbidden unless prior written permission is obtained from
PICO Technology Co., Ltd. 
*******************************************************************************/

using System;

namespace Unity.XR.PXR
{
    public class PXR_Enterprise
    {
        /// <summary>
        /// Init enterprise service.
        /// </summary>
        /// <returns>Whether the enterprise service has been initialized:
        /// * `true`: success
        /// * `false`: failure
        /// </returns>
        public static bool InitEnterpriseService()
        {
            return PXR_EnterprisePlugin.UPxr_InitEnterpriseService();
        }

        /// <summary>
        /// Open RGB camera.
        /// </summary>
        /// <returns>Whether the RGB camera has been opened:
        /// * `true`: success
        /// * `false`: failure
        /// </returns>
        public static bool OpenVSTCamera()
        {
            return PXR_EnterprisePlugin.UPxr_OpenVSTCamera();
        }

        /// <summary>
        /// Close RGB camera.
        /// </summary>
        /// <returns>Whether the RGB camera has been closed:
        /// * `true`: success
        /// * `false`: failure
        /// </returns>
        public static bool CloseVSTCamera()
        {
            return PXR_EnterprisePlugin.UPxr_CloseVSTCamera();
        }

        /// <summary>
        /// Get camera parameters(including intrinsics & extrinsics).
        /// </summary>
        /// <returns> RGBCameraParams including intrinsics and extrinsics.
        /// </returns>
        public static RGBCameraParams GetCameraParameters()
        {
            return PXR_EnterprisePlugin.UPxr_GetCameraParameters();
        }

        /// <summary>
        /// Get current head tracking confidence.
        /// </summary>
        /// <returns>
        /// * `0`: bad
        /// * `1`: good
        /// </returns>
        public static int GetHeadTrackingConfidence()
        {
            return PXR_EnterprisePlugin.UPxr_GetHeadTrackingConfidence();
        }

        /// <summary>
        /// Acquire RGB camera frame,anti-distortion
        /// </summary>
        /// <param name="width">[in]width desired frame width,should be less equal than 2328</param>
        /// <param name="height">[in]height desired frame height, should be less equal than 1748</param>
        /// <returns>frame info</returns>
        public static Frame AcquireVSTCameraFrameAntiDistortion(int width, int height)
        {
            return PXR_EnterprisePlugin.UPxr_AcquireVSTCameraFrameAntiDistortion(width, height);
        }

        /// <summary>
        /// Gets the predicted display time.
        /// <returns>The predicted display time.</returns>
        public static double GetPredictedDisplayTime()
        {
            return PXR_EnterprisePlugin.UPxr_GetPredictedDisplayTime();
        }

        /// <summary>
        /// Gets the predicted status of the sensor.
        /// </summary>
        /// <param name="predictTime">predict time.</param>
        /// <returns>The predicted status of the sensor.</returns>
        public static SensorState GetPredictedMainSensorState(double predictTime)
        {
            return PXR_EnterprisePlugin.UPxr_GetPredictedMainSensorState(predictTime);
        }
    }
}