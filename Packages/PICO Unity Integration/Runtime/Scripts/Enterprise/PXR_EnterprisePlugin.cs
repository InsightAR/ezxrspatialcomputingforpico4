﻿/*******************************************************************************
Copyright © 2015-2022 PICO Technology Co., Ltd.All rights reserved.  

NOTICE：All information contained herein is, and remains the property of 
PICO Technology Co., Ltd. The intellectual and technical concepts 
contained herein are proprietary to PICO Technology Co., Ltd. and may be 
covered by patents, patents in process, and are protected by trade secret or 
copyright law. Dissemination of this information or reproduction of this 
material is strictly forbidden unless prior written permission is obtained from
PICO Technology Co., Ltd. 
*******************************************************************************/

using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Unity.XR.PXR
{

    public enum PoseErrorType
    {
        BRIGHT_LIGHT_ERROR = (1 << 0),
        LOW_LIGHT_ERROR = (1 << 1),
        LOW_FEATURE_COUNT_ERROR = (1 << 2),
        CAMERA_CALIBRATION_ERROR = (1 << 3),
        RELOCATION_IN_PROGRESS = (1 << 4),
        INITILIZATION_IN_PROGRESS = (1 << 5),
        NO_CAMERA_ERROR = (1 << 6),
        NO_IMU_ERROR = (1 << 7),
        IMU_JITTER_ERROR = (1 << 8),
        UNKNOWN_ERROR = (1 << 9)
    };

    // pico slam results
    public struct SixDof
    {
        public Int64 timestamp;     // nanoseconds
        public double x;            // position X
        public double y;            // position Y
        public double z;            // position Z
        public double rw;           // rotation W
        public double rx;           // rotation X
        public double ry;           // rotation Y
        public double rz;           // rotation Z
        public byte type;           //1:6DOF 0:3DOF 
        public byte confidence;     //1:good 0:bad
        public PoseErrorType error;
        public double plane_height;
        public byte plane_status;
        public byte relocation_status;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        public byte[] reserved;
    }

    public struct AlgoResult
    {
        public SixDof pose;
        public SixDof relocation_pose;
        public double vx, vy, vz;        // linear velocity
        public double ax, ay, az;        // linear acceleration
        public double wx, wy, wz;        // angular velocity
        public double w_ax, w_ay, w_az;  // angular acceleration
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public byte[] reserved;
    }

    public struct FrameItem
    {
        public byte camera_id;
        public UInt32 width;                // width
        public UInt32 height;               // height
        public UInt32 format;               // format - rgb24
        public UInt32 exposure_duration;    // exposure duration:ns
        public UInt64 timestamp;            // start of exposure time:ns (BOOTTIME)
        public UInt64 qtimer_timestamp;     // nanoseconds in qtimer
        public UInt64 framenumber;          // frame number
        public UInt32 datasize;             // datasize
        public IntPtr data;                 // image data. 
    }

    public struct FrameItemExt
    {
        public FrameItem frame;
        public bool is_rgb;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public double[] rgb_tsw_matrix;
        public bool is_anti_distortion;
        public AlgoResult six_dof_pose;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] reserved;
    }

    public struct Frame
    {
        public UInt32 width;          // width
        public UInt32 height;         // height
        public UInt64 timestamp;      // start of exposure time:ns (BOOTTIME)
        public UInt32 datasize;       // datasize
        public IntPtr data;           // image data 
        public Pose pose;             // The head Pose at the time of image production.（Right-handed coordinate system: X right, Y up, Z in）
        public int status;            // sensor status(1:good 0:bad)
    }

    public struct SensorState
    {
        public Pose pose;             // Predict the head Pose at the screen up time.（Right-handed coordinate system: X right, Y up, Z in）
        public int status;            // sensor status(1:good 0:bad)
    }

    public struct RGBCameraParams
    {
        // Intrinsics
        public double fx;
        public double fy;
        public double cx;
        public double cy;
        // Extrinsics
        public double x;
        public double y;
        public double z;
        public double rw;
        public double rx;
        public double ry;
        public double rz;
    }


    public class PXR_EnterprisePlugin
    {
        private const string TAG = "[PXR_EnterprisePlugin]";
        public const int MAX_SIZE = 12208032;

        public static string token;
        private static FrameItemExt antiDistortionFrame;
        private static Frame frame;
        private static FrameItemExt distortionFrame;
        private static bool initDistortionFrame;
        private static int curSize = 0;

#if UNITY_ANDROID && !UNITY_EDITOR

        private static AndroidJavaClass BAuthLib;
        private static AndroidJavaClass unityPlayer;
        private static AndroidJavaObject currentActivity;        
#endif

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getHeadTrackingConfidence();

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int openVSTCamera();

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int closeVSTCamera();

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getHeadTrackingData(Int64 predictTime, ref SixDof data, int type);

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int acquireVSTCameraFrame(ref FrameItemExt out_frame);

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int acquireVSTCameraFrameAntiDistortion(string token, Int32 width, Int32 height, ref FrameItemExt frame);

        [DllImport("libpxr_xrsdk_native", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getCameraParameters(string token, ref RGBCameraParams rgb_Camera_Params);

        [DllImport("pxr_api", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Pxr_GetPredictedDisplayTime(ref double predictedDisplayTime);

        [DllImport("pxr_api", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Pxr_GetPredictedMainSensorState2(double predictTimeMs, ref PxrSensorState2 sensorState, ref int sensorFrameIndex);


        public static bool UPxr_InitEnterpriseService()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            BAuthLib = new AndroidJavaClass("com.pvr.tobauthlib.AuthCheckServer");
            unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
#endif
            return UPxr_GetToken();
        }

        private static bool UPxr_GetToken()
        {
            PLog.d(TAG, "GetToken Start");
#if UNITY_ANDROID && !UNITY_EDITOR
            token = BAuthLib.CallStatic<string>("featureAuthByToken", currentActivity, "getCameraInfo");
#endif
            if (string.IsNullOrEmpty(token))
            {
                PLog.e(TAG, "Failed to obtain token, camera data cannot be obtained!");
                return false;
            }
            PLog.d(TAG, "GetToken End token :" + token);
            return true;
        }

        public static int UPxr_GetHeadTrackingConfidence()
        {
            PLog.d(TAG, "GetHeadTrackingConfidence Start");
            int result = getHeadTrackingConfidence();
            PLog.d(TAG, "GetToken End result :" + result);

            return result;
        }

        public static bool UPxr_OpenVSTCamera()
        {
            PLog.d(TAG, "OpenVSTCamera Start");
            int result = openVSTCamera();
            PLog.d(TAG, "OpenVSTCamera End result :" + result);
            return result == 0;
        }

        public static bool UPxr_CloseVSTCamera()
        {
            PLog.d(TAG, "CloseVSTCamera Start");

            if (antiDistortionFrame.frame.data != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(antiDistortionFrame.frame.data);
                antiDistortionFrame.frame.data = IntPtr.Zero;
            }

            int result = closeVSTCamera();
            PLog.d(TAG, "CloseVSTCamera End result :" + result);
            return result == 0;
        }

        public static int UPxr_GetHeadTrackingData(Int64 predictTime, ref SixDof data, int type)
        {
            PLog.d(TAG, "GetHeadTrackingData Start");
            int result = getHeadTrackingData(predictTime, ref data, type);
            PLog.d(TAG, "GetHeadTrackingData End result :" + result);

            return result;
        }

        public static FrameItemExt UPxr_AcquireVSTCameraFrame()
        {
            PLog.d(TAG, "AcquireVSTCameraFrame Start");
            InitDistortionFrame();
            int result = acquireVSTCameraFrame(ref distortionFrame);
            PLog.d(TAG, "AcquireVSTCameraFrame End result :" + result);
            return distortionFrame;
        }

        public static Frame UPxr_AcquireVSTCameraFrameAntiDistortion(int width, int height)
        {
            PLog.d(TAG, "AcquireVSTCameraFrameAntiDistortion Start");
            if (string.IsNullOrEmpty(token))
            {
                PLog.e(TAG, "Failed to obtain token, camera data cannot be obtained!");
                return new Frame();
            }
            int size = width * height * 3;
            InitAntiDistortionFrame(size);
            int result = acquireVSTCameraFrameAntiDistortion(token, width, height, ref antiDistortionFrame);
            PLog.d(TAG, "AcquireVSTCameraFrameAntiDistortion End result :" + result);

            frame.width = antiDistortionFrame.frame.width;
            frame.height = antiDistortionFrame.frame.height;
            frame.timestamp = antiDistortionFrame.frame.timestamp;
            frame.datasize = antiDistortionFrame.frame.datasize;
            frame.data = antiDistortionFrame.frame.data;
            frame.data = antiDistortionFrame.frame.data;

            if (frame.pose != null)
            {
                frame.pose.position.x = (float)antiDistortionFrame.six_dof_pose.pose.x;
                frame.pose.position.y = (float)antiDistortionFrame.six_dof_pose.pose.y;
                frame.pose.position.z = (float)antiDistortionFrame.six_dof_pose.pose.z;
                frame.pose.rotation.w = (float)antiDistortionFrame.six_dof_pose.pose.rw;
                frame.pose.rotation.x = (float)antiDistortionFrame.six_dof_pose.pose.rx;
                frame.pose.rotation.y = (float)antiDistortionFrame.six_dof_pose.pose.ry;
                frame.pose.rotation.z = (float)antiDistortionFrame.six_dof_pose.pose.rz;
            }
            frame.status = antiDistortionFrame.six_dof_pose.pose.confidence;

            return frame;
        }

        public static Pose ToUnityPose(Pose poseR)
        {
            Pose poseL;
            poseL.position.x = poseR.position.x;
            poseL.position.y = poseR.position.y;
            poseL.position.z = -poseR.position.z;
            poseL.rotation.x = poseR.rotation.x;
            poseL.rotation.y = poseR.rotation.y;
            poseL.rotation.z = -poseR.rotation.z;
            poseL.rotation.w = -poseR.rotation.w;
            return poseL;
        }

        private static void InitDistortionFrame()
        {
            if (initDistortionFrame)
            {
                return;
            }
            distortionFrame = new FrameItemExt();
            if (distortionFrame.frame.data != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(antiDistortionFrame.frame.data);
                distortionFrame.frame.data = IntPtr.Zero;
            }
            distortionFrame.frame.data = Marshal.AllocHGlobal(MAX_SIZE);
            initDistortionFrame = true;
        }

        private static void InitAntiDistortionFrame(int size)
        {
            if (curSize == size)
            {
                return;
            }
            Debug.LogFormat("InitAntiDistortionFrame curSize={0}, size={1}", curSize, size);
            antiDistortionFrame = new FrameItemExt();
            if (antiDistortionFrame.frame.data != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(antiDistortionFrame.frame.data);
                antiDistortionFrame.frame.data = IntPtr.Zero;
            }
            antiDistortionFrame.frame.data = Marshal.AllocHGlobal(size);
            curSize = size;
            frame = new Frame();
        }

        public static RGBCameraParams UPxr_GetCameraParameters()
        {
            PLog.d(TAG, "GetCameraParameters Start");
            RGBCameraParams rgbCameraParams = new RGBCameraParams();
            if (string.IsNullOrEmpty(token))
            {
                PLog.e(TAG, "Failed to obtain token, camera data cannot be obtained!");
                return rgbCameraParams;
            }
            int result = getCameraParameters(token, ref rgbCameraParams);
            PLog.d(TAG, "GetCameraParameters End result :" + result);

            return rgbCameraParams;
        }

        public static double UPxr_GetPredictedDisplayTime()
        {
            PLog.i(TAG, "UPxr_GetPredictedDisplayTime()");
            double predictedDisplayTime = 0;
#if UNITY_ANDROID && !UNITY_EDITOR
            Pxr_GetPredictedDisplayTime(ref predictedDisplayTime);
#endif
            PLog.i(TAG, "UPxr_GetPredictedDisplayTime() predictedDisplayTime：" + predictedDisplayTime);
            return predictedDisplayTime;
        }

        public static SensorState UPxr_GetPredictedMainSensorState(double predictTime)
        {
            SensorState sensorState = new SensorState();
            PxrSensorState2 sensorState2 = new PxrSensorState2();
            int sensorFrameIndex = 0;
            Pxr_GetPredictedMainSensorState2(predictTime, ref sensorState2, ref sensorFrameIndex);
            sensorState.status = sensorState2.status == 3 ? 1 : 0;
            sensorState.pose.position.x = sensorState2.globalPose.position.x;
            sensorState.pose.position.y = sensorState2.globalPose.position.y;
            sensorState.pose.position.z = sensorState2.globalPose.position.z;
            sensorState.pose.rotation.x = sensorState2.globalPose.orientation.x;
            sensorState.pose.rotation.y = sensorState2.globalPose.orientation.y;
            sensorState.pose.rotation.z = sensorState2.globalPose.orientation.z;
            sensorState.pose.rotation.w = sensorState2.globalPose.orientation.w;
            return sensorState;
        }
    }
}